classdef MCMCSampler < handle
    %MCMCSAMPLER MCMC chain and sampler class
    %   Vassilios Stathopoulos, stathv@gmail.com
    %   September 2012
    %   
    %   Constructors:
    %   rmhmc_sampler = NewRMHMCSampler(target, varargin)
    %       Construct a new RMHMC sampler.
    %       target      : Target density, an object implementing the
    %                     TargetDistribution interface.
    %       varargin    : variable length property value pairs.
    %                     Valid property names:
    %             'Samples'          : number of posterior samples. 
    %                                  Default is 1000.
    %             'BurnIn'           : number og burn-in samples.
    %                                  Default is 500.
    %             'Verbose'          : iterations before displaying sampler 
    %                                  statistics. Default is 100.
    %             'VerboseLevel'     : set verbosity level. Default 'some'.
    %                                  'none' no display.
    %                                  'some' display acceptance rate and
    %                                  number of samples.
    %                                  'all'  display adaptation  
    %                                  information during burn-in.
    %             'Adapt'            : number of iterations for adapting
    %                                  MCMC kernel parameters during
    %                                  burn-in. Default is 100.
    %             'FixedPointSteps'  : Number of fixed-point Newton iterations
    %                                  for implicit updates.
    %             'StepSize'         : Integration step size.
    %             'LeapFrogSteps'    : Number of leap-frog integration steps.
    %             'Init'             : Initial values for the chain.                   
    %
    %   ermlmc_sampler = NewERMLMCSampler(target, varargin)
    %       Construct a new eRMLMC sampler.
    %       target      : Target density, an object implementing the
    %                     TargetDistribution interface.
    %       varargin    : variable length property value pairs.
    %                     Valid property names:
    %             'Samples'          : number of posterior samples. 
    %                                  Default is 1000.
    %             'BurnIn'           : number og burn-in samples.
    %                                  Default is 500.
    %             'Verbose'          : iterations before displaying sampler 
    %                                  statistics. Default is 100.
    %             'VerboseLevel'     : set verbosity level. Default 'some'.
    %                                  'none' no display.
    %                                  'some' display acceptance rate and
    %                                  number of samples.
    %                                  'all'  display adaptation  
    %                                  information during burn-in.
    %             'Adapt'            : number of iterations for adapting
    %                                  MCMC kernel parameters during
    %                                  burn-in. Default is 100.
    %             'StepSize'         : Integration step size.
    %             'LeapFrogSteps'    : Number of leap-frog integration steps.
    %             'Init'             : Initial values for the chain.                   
    %
    %   rmlmc_sampler = NewRMLMCSampler(target, varargin)
    %       Construct a new RMLMC sampler.
    %       target      : Target density, an object implementing the
    %                     TargetDistribution interface.
    %       varargin    : variable length property value pairs.
    %                     Valid property names:
    %             'Samples'          : number of posterior samples. 
    %                                  Default is 1000.
    %             'BurnIn'           : number og burn-in samples.
    %                                  Default is 500.
    %             'Verbose'          : iterations before displaying sampler 
    %                                  statistics. Default is 100.
    %             'VerboseLevel'     : set verbosity level. Default 'some'.
    %                                  'none' no display.
    %                                  'some' display acceptance rate and
    %                                  number of samples.
    %                                  'all'  display adaptation  
    %                                  information during burn-in.
    %             'Adapt'            : number of iterations for adapting
    %                                  MCMC kernel parameters during
    %                                  burn-in. Default is 100.
    %             'FixedPointSteps'  : Number of fixed-point Newton iterations
    %                                  for implicit updates.
    %             'StepSize'         : Integration step size.
    %             'LeapFrogSteps'    : Number of leap-frog integration steps.
    %             'Init'             : Initial values for the chain.     
    %
    %   Methods:
    %       [post tt ar burnin] = MCMCSample()
    %           Get samples from the Markov chain.
    %           post  : posterior samples
    %           tt    : time take to sample posterior samples.
    %           ar    : average acceptance rate during posterior sampling.
    %           burnin: burn-in samples. 
    %           To reduce memory ussage especially for long burn-in
    %           sessions use: [post tt ar] = MCMCSample()
    %
    %       printStats()
    %           Print sampler statistics on the console.
    %           
    %       kernelParams = getKernelParameters()
    %           Return a structure with MCMC kernel parameters such as:
    %           StepSize and LeapFrogSteps.
    %
    %   Properties:
    %       posteriorSamples      
    %       burnInSamples        
    %       verboseIterations
    %       verboseLevel
    %       numberOfParameters
    %       adaptIterations
        
    properties (SetAccess = private)
        posteriorSamples;
        burnInSamples;
        verboseIterations;
        verboseLevel;
        numberOfParameters;
        adaptIterations;
    end
    
    properties (Access = private)
        mcmc;
        verboseLevels = {'none','some','all'};
    end
    
    % Constructor is private and used only by Factory methods
    methods (Access = private)
        
        function self = MCMCSampler(postSamp, biSamp, verbIt, verbL, adapt, kernel)
            if ~isa(kernel,'MCMCKernel')
                error('MCMCSampler:BadConstruction', ...
                    'Kernel should be an instanse of MCMCKernel');
            end
            
            if  ~any(strcmp(verbL, self.verboseLevels))
                error('MCMCSampler:BadConstruction', ...
                    ['verbose level must be one of ' ...
                    cell2mat(cellfun(@(x) [x ' '], self.verboseLevels,'UniformOutput',false))]);
            end
            
            self.mcmc = kernel;
            self.numberOfParameters = kernel.NumberOfParameters;
            
            self.posteriorSamples = postSamp;
            self.burnInSamples = biSamp;
            self.verboseIterations = verbIt;
            self.verboseLevel = find(strcmp(verbL, self.verboseLevels));
            self.adaptIterations = adapt;
        end
        
        
        function VerboseOutput(self, accepted, n)
            % accepted = [total verbose]
            
            % verbose level > none
            if self.verboseLevel > 1
                accRate = accepted(1)/n;
                fprintf(1,'%u samples complete. Total acceptance rate %g\n',n, accRate);
            end
            
            % verbose level > none
            if self.verboseLevel > 1
                
                accRate = accepted(2)/self.verboseIterations;
                fprintf(1,'Acceptance rate for last %u samples %g.\n',...
                    self.verboseIterations, accRate);
            end
            
            % verbose level > some and no kernel adaptation
            if (self.verboseLevel > 2 && self.adaptIterations < 1)
                fprintf(1,'==== Kernel Stats ====\n');
                self.mcmc.printStats();
                fprintf(1,'======================\n');
            end
        end
        
        
        function KernelAdaptation(self, accepted, n)
            % verbose level > some
            if self.verboseLevel > 2
                fprintf(1,'==== Kernel Stats before adaptation ====\n');
                self.mcmc.printStats();
                fprintf(1,'========================================\n');
            end
            
            accRate = accepted/self.adaptIterations;
            self.mcmc.adapt(accRate);
            
            % verbose level > none
            if self.verboseLevel > 1
                fprintf(1,'Adapting kernel, acceptance rate %g \n', accRate);
            end
            
            % verbose level > some
            if self.verboseLevel > 2
                fprintf(1,'==== Kernel Stats after adaptation ====\n');
                self.mcmc.printStats();
                fprintf(1,'========================================\n');
            end
        end
        
    end
    
    %public methods
    methods
        function [post tt ar burnin] = MCMCSample(self)
            
            % ==== BurnIn
            % preallocate memmory
            if nargout > 3
                burnin = zeros(self.burnInSamples , self.numberOfParameters);
            end
            
            %accepted = [total adapt verbose]
            accepted = [0     0     0];
            
            for n = 1:self.burnInSamples
                
                [x acc] = self.mcmc.nextSample();
                
                if acc
                    accepted = accepted + 1;
                end
                
                % save burnin samples
                if nargout > 3
                    burnin(n,:) = x';
                end
                
                % kernel adaptation
                if (self.adaptIterations > 0 && mod(n,self.adaptIterations) == 0)
                    self.KernelAdaptation(accepted(2),n);
                    accepted(2) = 0;
                end
                
                % verbose output
                if mod(n, self.verboseIterations) == 0
                    self.VerboseOutput(accepted([1 3]),n);
                    accepted(3) = 0;
                end
                
            end
            
            % verbose level > none
            if self.verboseLevel > 1
                fprintf(1,'================================================\n');
                fprintf(1,'Burn-in complete, now drawing posterior samples.\n');
                fprintf(1,'================================================\n');
            end
            % ==== End BurnIn
            
            
            % ==== Posterior Sampling
            % prealocate memmory
            post = zeros(self.posteriorSamples, self.numberOfParameters);
            
            %accepted = [total verbose]
            accepted = [0 0];
            
            timer = tic;
            for n = 1:self.posteriorSamples
                [x acc] = self.mcmc.nextSample();
                
                if acc
                    accepted = accepted + 1;
                end
                
                %save
                post(n,:) = x';
                
                % verbose output
                if mod(n, self.verboseIterations) == 0
                    self.VerboseOutput(accepted,n);
                    accepted(2) = 0;
                end
            end
            
            tt = toc(timer);
            
            ar = accepted(1)/self.posteriorSamples;
        end
        
        
        function printStats(self)
            self.mcmc.printStats();
        end
        
        function kernelParams = getKernelParameters(self)
            kernelParams = self.mcmc.getParams();
        end
    end
    
    
    methods (Static)
        %%%%%%%%%%%%%%%%%%%%%%%%%%
        % Static factory methods %
        %%%%%%%%%%%%%%%%%%%%%%%%%%
         
        % RMHMC sampler
        function rmhmc_sampler = NewRMHMCSampler(target, varargin)
            
            [Samples BurnIn Verbose VerboseLevel Adapt] = MCMCSampler.ParseParams(varargin{:});
            
            rmhmc_kernel = RMHMCKernel.NewRMHMCKernel(target, varargin{:});
            
            rmhmc_sampler = MCMCSampler(Samples, BurnIn, Verbose,...
                VerboseLevel, Adapt, rmhmc_kernel);
        end
        
        % Explicit RMLMC sampler
        function ermlmc_sampler = NewERMLMCKernel(target, varargin)
            
            [Samples BurnIn Verbose VerboseLevel Adapt] = MCMCSampler.ParseParams(varargin{:});
            
            ermlmc_kernel = eRMLMCKernel.NewERMLMCKernel(target, varargin{:});
            
            ermlmc_sampler = MCMCSampler(Samples, BurnIn, Verbose,...
                VerboseLevel, Adapt, ermlmc_kernel);
        end
        
        % Semi-explicit RMLMC sampler
        function rmlmc_sampler = NewRMLMCSampler(target, varargin)
            
            [Samples BurnIn Verbose VerboseLevel Adapt] = MCMCSampler.ParseParams(varargin{:});
            
            rmlmc_kernel = RMLMCKernel.NewRMLMCKernel(target, varargin{:});
            
            rmlmc_sampler = MCMCSampler(Samples, BurnIn, Verbose,...
                VerboseLevel, Adapt, rmlmc_kernel);
        end
     
        % HMC sampler
        function hmc_sampler = NewHMCSampler(target, varargin)
            
            [Samples BurnIn Verbose VerboseLevel Adapt] = MCMCSampler.ParseParams(varargin{:});
            
            hmc_kernel = HMCKernel.NewHMCKernel(target, varargin{:});
            
            hmc_sampler = MCMCSampler(Samples, BurnIn, Verbose,...
                VerboseLevel, Adapt, hmc_kernel);
        end
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%
        % End of factory methods %
        %%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    
    methods (Static, Access = private)
        function [Samples BurnIn Verbose VerboseLevel Adapt] = ParseParams(varargin)
            
            argP = inputParser;
            argP.KeepUnmatched = true;
            argP.addParamValue('Samples', 1000, @(x) x > 0 );
            argP.addParamValue('BurnIn', 500, @(x) x >= 0 );
            argP.addParamValue('Verbose', 100,  @(x) x >= 0 );
            argP.addParamValue('VerboseLevel', 'some');
            argP.addParamValue('Adapt', 100);
            argP.parse( varargin{:} );
            
            Samples = argP.Results.Samples;
            BurnIn =  argP.Results.BurnIn;
            Verbose = argP.Results.Verbose;
            VerboseLevel = argP.Results.VerboseLevel;
            Adapt = argP.Results.Adapt;
            
        end
    end
    
end

