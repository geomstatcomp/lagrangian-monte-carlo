for Dataset =  {'Australian' 'German' 'Heart' 'Pima' 'Ripley'}
    disp(['Running with ' Dataset{1} ' dataset'])
    cd('./MCMC/')
    BLR_sLMC(Dataset{1})
    cd('./Results/')
    CalculateStatistics('Results_sLMC', Dataset{1})
    cd('..')
end