%   TargetDistribution Abstract class (intefrace) for a target distribution
%   Vassilios Stathopoulos, stathv@gmail.com
%   Semptember 2012.
%
%   LogLikelihood                -- Joint log likelihood function.
%   LogLikelihoodGradient        -- Gradient of the joint log likelihood.
%   MetricTensor                 -- Metric Tensor function which returns a 
%                                   square, positive definite matrix.
%   MetricTensorDerivative       -- Function that calculates the partial 
%                                   derivatives of the Metric Tensor in a 
%                                   cell array.
%   InitParameters               -- Returns a vector containing initial 
%                                   parameter values.
%    
%   samplePrior                  -- Returns a sample from the prior.
%   Constraints                  -- Retunrs whether contraints over 
%                                   the parameters are satisfied or not.

classdef TargetDistribution < handle
    
    properties (Abstract = true)
        InitParameters;
    end % abstract properties
    
    methods (Abstract = true)
        JLL = LogLikelihood(self, params);
    end % abstract methods    
    
    methods
        function newX = samplePrior(self,numOfSamples) %#ok<STOUT,MANU,INUSD>
            error('TargetDistribution:NoPrior','Prior sample not implemented');
        end
        
        function dJLL = LogLikelihoodGradient(self, params) %#ok<STOUT,MANU,INUSD>
            error('TargetDistribution:NoGradientInformation','Metric Tensor Not Implemented');
        end
    
        function M = MetricTensor(self, params) %#ok<STOUT,MANU,INUSD>
            error('TargetDistribution:NoMetricInformation','Metric Tensor Not Implemented');
        end
        
        function dM = MetricTensorDerivative(self, params) %#ok<STOUT,MANU,INUSD>
             error('TargetDistribution:NoMetricInformation','Metric Tensor Not Implemented');        
        end
        
        function valid = Constraints(self, params) %#ok<MANU,INUSD>
            valid = true;
        end
    end % methods    
    
end % classdef