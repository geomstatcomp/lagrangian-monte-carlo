function [ ] = BLR_RHMC( DataSet )

% Random Numbers...
randn('state', 2012);
rand('twister', 2012);

switch(DataSet)
    
    case 'Australian'
    
        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/australian.mat');
        t=X(:,end);
        X(:,end)=[];
        
        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);

        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 6;
        StepSize           = 4.5/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
    case 'German'

        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/german.mat');
        t=X(:,end);
        X(:,end)=[];

        % German Credit - replace all 1s in t with 0s
        t(find(t==1)) = 0;
        % German Credit - replace all 2s in t with 1s
        t(find(t==2)) = 1;

        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 6;
        StepSize           = 4/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
    case 'Heart'
        
        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/heart.mat');
        t=X(:,end);
        X(:,end)=[];

        % German Credit - replace all 1s in t with 0s
        t(find(t==1)) = 0;
        % German Credit - replace all 2s in t with 1s
        t(find(t==2)) = 1;
        
        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 6;
        StepSize           = 4.5/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;

        
    case 'Pima'

        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/pima.mat');
        t=X(:,end);
        X(:,end)=[];

        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 6;
        StepSize           = 5/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
        
        
    case 'Ripley'

        %Two hyperparameters of model
        Polynomial_Order = 3;
        alpha=100;

        %Load and prepare train & test data
        load('./Data/ripley.mat');
        t=X(:,end);
        X(:,end)=[];
        
        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end
        
        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 6;
        StepSize           = 3.5/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
    case 'CTG'
    
        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        %data set is already standardized
        load('./Data/CTG.mat');
        t = y;
        XX = x;
        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 6;
        StepSize           = 3/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
end


Proposed = 0;
Accepted = 0;

% Set initial values of w
w = ones(D,1)*1e-3;
wSaved = zeros(NumOfIterations-BurnIn,D);
% loglik = zeros(NumOfIterations-BurnIn,1);
warning off

% Pre-allocate memory for partial derivatives
for d = 1:D
    GDeriv{d} = zeros(D);
end

% Calculate joint log likelihood for current w
CurrentU    = U(t,XX,w,alpha);


for IterationNum = 1:NumOfIterations
    
    if mod(IterationNum,100) == 0
        disp([num2str(IterationNum) ' iterations completed.'])
        disp(Accepted/Proposed)
        Proposed = 0;
        Accepted = 0;
        drawnow
    end
    
    %IterationNum
    wNew = w;
    Proposed = Proposed + 1;
    
    % random number of leapfrog steps
    if (randn > 0.5) TimeStep = 1; else TimeStep = -1; end
%    RandomSteps = ceil(rand*NumOfLeapFrogSteps);
    SavedSteps(1,:) = wNew;
    
    % pre-leapfrog calculation
    % Calculate G
    G     = Met(XX,wNew,alpha);
    CholG = chol(G);
    InvG  = inv(G);
    % Calculate the partial derivatives dG/dw
    GDeriv = Met(XX,wNew,alpha,1);
    for d = 1:D
        InvGdG{d}      = InvG*GDeriv{d};
        TraceInvGdG(d) = trace(InvGdG{d});
    end
    % terms other than quadratic one
    dphi = U(t,XX,wNew,alpha,1) + 0.5*TraceInvGdG';
    
    % propose momentum
    Momentum = (randn(1,D)*CholG)';
    
    % Calculate current H value
    CurrentLogDet = sum(log(diag(CholG)));
    CurrentH  = CurrentU + CurrentLogDet + (Momentum'*InvG*Momentum)/2;
    
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        % Multiple fixed point iteration
        PM = Momentum;
        for FixedIter = 1:NumOfNewtonSteps
            MomentumHist(FixedIter,:) = PM;
            
            InvGMomentum = InvG*PM;
            for d = 1:D
                dQuadTerm(d)  = 0.5*(PM'*InvGdG{d}*InvGMomentum);
            end
            
            PM = Momentum + TimeStep*(StepSize/2)*(-dphi + dQuadTerm');
        end
        Momentum = PM;
        
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update w parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        %%% Multiple Fixed Point Iteration %%%
        FixedInvGMomentum  = G\Momentum;
        
        Pw = wNew;
        for FixedIter = 1:NumOfNewtonSteps
            wHist(FixedIter,:) = Pw;
            
            InvGMomentum = Met(XX,Pw,alpha)\Momentum;
            Pw = wNew + (TimeStep*(StepSize/2))*(FixedInvGMomentum + InvGMomentum);
        end
        wNew = Pw;
        
        
        % Update G based on new parameters
        G = Met(XX,wNew,alpha);
        InvG = inv(G);
        % Update the partial derivatives dG/dw
        GDeriv = Met(XX,wNew,alpha,1);
        for d = 1:D
            InvGdG{d}      = InvG*GDeriv{d};
            TraceInvGdG(d) = trace(InvGdG{d});
        end
        % terms other than quadratic one
        dphi = U(t,XX,wNew,alpha,1) + 0.5*TraceInvGdG';
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        InvGMomentum = InvG*Momentum;
        for d = 1:D
            dQuadTerm(d) = 0.5*(Momentum'*InvGdG{d}*InvGMomentum);
        end
        Momentum = Momentum + TimeStep*(StepSize/2)*(-dphi + dQuadTerm');
        
        % save all proposals
        SavedSteps( StepNum + 1, : ) = wNew;
        
        
    end
    
    try
        % Calculate proposed H value
        ProposedLogDet = sum(log(diag(chol(G))));
        ProposedU = U(t,XX,wNew,alpha);
    
        ProposedH = ProposedU + ProposedLogDet + (Momentum'*InvG*Momentum)/2;
    
        % Accept according to ratio
        Ratio = -ProposedH + CurrentH;
    
        if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
            CurrentU = ProposedU;
            w = wNew;
            Accepted = Accepted + 1;
            %        disp('Accepted')
        else
            %        disp('Rejected')
        end
    catch
        disp('Rejected');
        drawnow
    end
    
    
    % Save samples if required
    if IterationNum > BurnIn
        wSaved(IterationNum-BurnIn,:) = w;
%         f = XX*w; loglik(IterationNum-BurnIn) = f'*t - sum(log(1+exp(f)));
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing posterior samples.')
        tic;
    end
    
end

% Stop timer
TimeTaken = toc
betaPosterior = wSaved;
acprat = size(unique(betaPosterior,'rows'),1)/(NumOfIterations-BurnIn)

CurTime = fix(clock);
cd('..')
save(['Results/Results_RHMC_lr_' num2str(NumOfLeapFrogSteps) '_' num2str(NumOfNewtonSteps) '_' DataSet '_' num2str(CurTime) '.mat'], 'StepSize', 'NumOfLeapFrogSteps', 'betaPosterior', 'acprat', 'TimeTaken')

% Plot paths and histograms
%figure(1)
%plot(wSaved);
%figure(2)
%for d = 1:D
%    subplot(ceil(D/4),4,d)
%    hist(wSaved(:,d))
%end



end




function [output]=U(t,XX,w,alpha,der)
if(nargin<5)
    der=0;
end

f  = XX*w;
D = length(w);

if der==0
    % Calculate joint log likelihood for w
%     LogPrior      = -ecmnobj(w',zeros(D,1),alpha*eye(D));
    LogPrior      = -w'*w/2/alpha;
    LogLikelihood = f'*t - sum(log(1+exp(f))); %training likelihood
    output    = -(LogLikelihood + LogPrior);
elseif der==1
    % Calculate gradient of joint log likelihood for w
    dLogPrior      = - (1/alpha)*w ;
    dLogLikelihood = XX'*( t - (1./(1+exp(-f))));
    output    = -(dLogLikelihood + dLogPrior);
else
    disp('wrong choice of der!')
end

end




function [output]=Met(XX,w,alpha,der)
if(nargin<4)
    der=0;
end

[N,D] = size(XX);
f = XX*w;
p = 1./(1+exp(-f));
v = p.*(ones(N,1)-p);

% Calculate G
if der==0
    G = (XX'.*repmat(v',D,1))*XX + (eye(D)./alpha);
    output = G;
% Calculate the partial derivatives dG/dw
% Faster to compute Z as vector then diag in the next calculation
elseif der==1
    for d = 1:D
        Z = ((1 - 2*p).*XX(:,d));
        %GDeriv{d} = XX'*v*diag(Z)*XX; % Slow because of diag
        % faster
        Z1 = (v.*Z);
        for a =1:D Z2(:,a) = (XX(:,a).*Z1); end
        GDeriv{d} = Z2'*XX;
    end
    output = GDeriv;
else
    disp('wrong choice of der!')
end


end