%%% This is to plot efficiency meansured by minESS/s vs the complexity of
%%% the problem


% set rng seed
s = RandStream('mt19937ar','Seed',2013);
RandStream.setGlobalStream(s);

D = 5;
K = 5;

Eff_K=zeros(4,K);
ESS_K=zeros([4,K,4]);
Eff_D=zeros(4,D);
ESS_D=zeros([4,D,4]);


currentfolder=cd('./Results/');
% HMC
for k=1:K
    [Eff_K(1,k),ESS_K(1,k,1),ESS_K(1,k,2),ESS_K(1,k,3),ESS_K(1,k,4)]=CalculateStatistics('SimT_hmc',strcat('D',num2str(20),'_K',num2str(10^k,'%.0e')));
    [Eff_D(1,k),ESS_D(1,k,1),ESS_D(1,k,2),ESS_D(1,k,3),ESS_D(1,k,4)]=CalculateStatistics('SimT_hmc',strcat('D',num2str(k*10),'_K',num2str(10^4,'%.0e')));
end

% rmhmc
for k=1:K
    [Eff_K(2,k),ESS_K(2,k,1),ESS_K(2,k,2),ESS_K(2,k,3),ESS_K(2,k,4)]=CalculateStatistics('SimT_rhmc',strcat('D',num2str(20),'_K',num2str(10^k,'%.0e')));
    [Eff_D(2,k),ESS_D(2,k,1),ESS_D(2,k,2),ESS_D(2,k,3),ESS_D(2,k,4)]=CalculateStatistics('SimT_rhmc',strcat('D',num2str(k*10),'_K',num2str(10^4,'%.0e')));
end

% slmc
for k=1:K
    [Eff_K(3,k),ESS_K(3,k,1),ESS_K(3,k,2),ESS_K(3,k,3),ESS_K(3,k,4)]=CalculateStatistics('SimT_slmc',strcat('D',num2str(20),'_K',num2str(10^k,'%.0e')));
    [Eff_D(3,k),ESS_D(3,k,1),ESS_D(3,k,2),ESS_D(3,k,3),ESS_D(3,k,4)]=CalculateStatistics('SimT_slmc',strcat('D',num2str(10*k),'_K',num2str(10^4,'%.0e')));
end

% lmc
for k=1:K
    [Eff_K(4,k),ESS_K(4,k,1),ESS_K(4,k,2),ESS_K(4,k,3),ESS_K(4,k,4)]=CalculateStatistics('SimT_lmc',strcat('D',num2str(20),'_K',num2str(10^k,'%.0e')));
    [Eff_D(4,k),ESS_D(4,k,1),ESS_D(4,k,2),ESS_D(4,k,3),ESS_D(4,k,4)]=CalculateStatistics('SimT_lmc',strcat('D',num2str(10*k),'_K',num2str(10^4,'%.0e')));
end
cd(currentfolder);


% plot
fig1=figure(1); clf;
set(fig1,'windowstyle','docked');
set(0,'DefaultAxesLineStyleOrder','-|--|:|-.');

% efficiency vs K
subplot(1,2,1);

m = {'+','o','s','x','*','.','d','^','v','>','<','p','h'};
set(gca(), ...
    'LineStyleOrder',m, 'ColorOrder',[0 0 0], ...
    'NextPlot','replacechildren');

set(gca,'box','on');
eff_K=plot(Eff_K','LineWidth',1.5,'Markersize',8);
ylim([0,850]);
set(gca,'Xtick',1:K,'Xticklabel',arrayfun(@(x)regexprep(num2str(x,'%.0e'), '(e[+-])0(\d)', '$1$2'),10.^(1:K),'UniformOutput',false));
c = num2cell(get(0,'DefaultAxesColorOrder'),2);
l = cellstr(get(0,'DefaultAxesLineStyleOrder'));
set(eff_K, {'Color'}, c(rem((1:numel(eff_K))-1,numel(c))+1), ...
    {'LineStyle'}, l(rem((1:numel(eff_K))-1,numel(l))+1));
title('D = 20','Fontsize',20);
set(gca,'FontSize',12);
ylabel('Min(ESS)/s','Fontsize',15);
xlabel('$\lambda_{\max}\;/\lambda_{\min}$','interpreter','latex','Fontsize',15);
% xlabh = get(gca,'XLabel');
% set(xlabh,'Position',get(xlabh,'Position') + [0 20 0]);
leg=legend('HMC','RHMC','sLMC','LMC','Location','NorthEast');
set(leg,'Fontsize',15);


% efficiency vs D
subplot(1,2,2);

m = {'+','o','s','x','*','.','d','^','v','>','<','p','h'};
set(gca(), ...
    'LineStyleOrder',m, 'ColorOrder',[0 0 0], ...
    'NextPlot','replacechildren');

set(gca,'box','on');
eff_D=plot(Eff_D','LineWidth',1.5,'Markersize',8);
ylim([0,450]);
set(gca,'Xtick',1:D,'Xticklabel',num2str(10.*(1:D)'));
c = num2cell(get(0,'DefaultAxesColorOrder'),2);
l = cellstr(get(0,'DefaultAxesLineStyleOrder'));
set(eff_D, {'Color'}, c(rem((1:numel(eff_D))-1,numel(c))+1), ...
    {'LineStyle'}, l(rem((1:numel(eff_D))-1,numel(l))+1));
title('$\lambda_{\max}\;/\lambda_{\min} = 10000$','interpreter','latex','Fontsize',20);
set(gca,'FontSize',12);
ylabel('Min(ESS)/s','Fontsize',15);
xlabel('Dimension','Fontsize',15);
% xlabh = get(gca,'XLabel');
% set(xlabh,'Position',get(xlabh,'Position') + [0 20 0]);
leg=legend('HMC','RHMC','sLMC','LMC','Location','NorthEast');
set(leg,'Fontsize',15);

