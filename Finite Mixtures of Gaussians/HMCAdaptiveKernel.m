classdef HMCAdaptiveKernel < handle
    %HMCADAPTIVEKERNEL Common adaptive tunning functionality for 
    %  all dynamical Monte Carlo algorithms.
    
    properties
        StepSize;
        LeapFrogSteps;
        TargetPathDist = 3.5;
    end
    
    properties (Abstract, Constant)
        TargetAccRate;
    end
    
    methods
         function a = adapt(self, accRate)            
            aSize = 1./(1+exp(-11*(accRate-self.TargetAccRate))) + 0.5;
            self.StepSize = self.StepSize*aSize;
            
            self.LeapFrogSteps = ceil(self.TargetPathDist/self.StepSize);
            if self.LeapFrogSteps > 200
                self.LeapFrogSteps = 200;
            end
            
            a = self.StepSize;
         end
        
        function  paramsStruct = getParams(self)
            paramsStruct.StepSize = self.StepSize;
            paramsStruct.LeapFrogSteps = self.LeapFrogSteps;
        end
    end
    
end

