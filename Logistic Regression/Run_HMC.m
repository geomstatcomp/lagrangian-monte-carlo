for Dataset =  {'Australian' 'German' 'Heart' 'Pima' 'Ripley'}
    disp(['Running with ' Dataset{1} ' dataset'])
    cd('./MCMC')
    BLR_HMC(Dataset{1})
    cd('./Results/')
    CalculateStatistics('Results_HMC', Dataset{1})
    cd('..')
end
