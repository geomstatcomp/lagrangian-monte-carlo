classdef RMHMCKernel < MCMCKernel & HMCAdaptiveKernel
    % RMHMCKernel Riemann Manifold Hamiltonian Monte Carlo kernel
    %   Vassilios Stathopoulos, stathv@gmail.com
    %   September 2012
    %
    %  Constructors:
    %       rmhmc_kernel = NewRMHMCKernel(target)
    %           Constructs a new RMHMCKernel with step-size 0.15, 6
    %           leap-frog integration steps, 10 steps of fixed-point Newton 
    %           iterations for implicit updates and arbitary initial values.
    %           target    : Target density, an object implementing 
    %                       the TargetDistribution interface.
    %
    %       rmhmc_kernel = NewRMHMCKernel(target, PropertyName,Value, ... )
    %           Construct a new RMHMCKernel with properties spesified as
    %           parameters. Valid property names are:
    %           'FixedPointSteps'  : Number of fixed-point Newton iterations
    %                                for implicit updates.
    %           'StepSize'         : Integration step size.
    %           'LeapFrogSteps'    : Number of leap-frog integration steps.
    %           'Init'             : Initial values for the chain.
    %       Example: 
    %           rmhmc_kernel = NewRMHMCKernel(myTarget, 'StepSize',0.5);
    %           
    %       RMHMCKernel(Target,StepSize,LeapFrogSteps,FixedPointIterations,Init)
    %           Target                : The target density.
    %           StepSize              : Integration step size.
    %           LeapFrogStep          : Number of leap-frog integration 
    %                                   steps.
    %           FixedPointIterations  : Number of fixed-point Newton 
    %                                   iterations for implicit updates.
    %           Init                  : Initial values for the chain.
    %
    %  Properties:
    %       State_x              : Current value of the chain.
    %       State_LogPosterior   : Value of the target at State_x.
    %       Target               : Target density.
    %       NumberOfParameters   : Length of State_x.
    %       FixedPointSteps      : Number of fixed-point Newton iterations 
    %                              for implicit updates.
    %       TargetAccRate        : Optimal acceptance rate. Used only for
    %                              tunning.
    %       cE                   : Error tollerace for convergence of
    %                              implicit equations.
    %
    %   Methods:
    %       [x acc] = nextSample()
    %           Returns a new random sample from the target density.
    %           x:      The new sample.
    %           acc:    1 if x is a new sample (accepted proposal) and 0 if
    %                   x is the same as the previous sample in the chain
    %                   (rejected proposal).
    %
    %       printStats()
    %           Prints kernel statistics, step-size, number of leap-frog
    %           integration steps and current value of the target density on
    %           the console.
    %
    
    properties
        State_x;
        State_LogPosterior;
        Target;
        FixedPointSteps;
    end
    
    properties (Dependent)
        NumberOfParameters;
    end
    
    properties (Constant)
        TargetAccRate = 0.82;
        cE = 1e-4;
    end
    
    properties (Access = private);
        G;
        invG;
        cholG;
        dG;
        invGdGinvG;
        trinvGdG;
    end
    
    methods
        function self = RMHMCKernel(Target,StepSize,LeapFrogSteps,FixedPoint,Init)
            if ~isa(Target,'TargetDistribution')
                error('RMHMCKernel:BadConstruction', ...
                    'target should be an instance of TargetDistribution');
            end
            
            if length(Target.InitParameters)~=length(Init)
                error('RMHMCKernel:BadConstruction', ...
                    'Init must have the same size as Target.InitParamaters');
            end
            
            self.Target = Target;
            self.State_x = Init;
            
            self.StepSize = StepSize;
            self.LeapFrogSteps = LeapFrogSteps;
            
            self.FixedPointSteps = FixedPoint;
            
            % initial condition
            self.State_LogPosterior = Target.LogLikelihood(self.State_x);
            self.G = Target.MetricTensor(self.State_x);
            D = self.NumberOfParameters;
            self.cholG = chol(self.G);
            self.invG = self.cholG\(self.cholG'\eye(D));
            
            self.dG = Target.MetricTensorDerivative(self.State_x);
            for d= 1:D
                self.invGdGinvG(:,:,d)     = self.invG*self.dG{d};
                self.trinvGdG(d)   = trace(self.invGdGinvG(:,:,d));
                self.invGdGinvG(:,:,d) = self.invGdGinvG(:,:,d) *self.invG;
            end
            
        end
        
        function NumberOfParameters = get.NumberOfParameters(self)
            NumberOfParameters = length(self.State_x);
        end
        
        function [x acc E] = nextSample(self)
            acc = 0;
            x = self.State_x;
            
            
            D = self.NumberOfParameters;
            
            
            xNew = self.State_x;
            Gnew = self.G;
            cholGnew = self.cholG;
            invGnew = self.invG;
            dGnew = self.dG;
            invGdGinvGnew = self.invGdGinvG;
            trinvGdGnew = self.trinvGdG;
            
            % sample momenta
            p = ( randn(1, D) * cholGnew )';
            pNew = p;
            
            % Randomise number of leap-frog steps.
            RandomSteps = ceil(rand*self.LeapFrogSteps);
            pTinvGdGinvGp = zeros(1,D);
            
            if nargout > 2
                E = zeros(RandomSteps,1);
            end
            
            for StepNum = 1:RandomSteps
                
                % Fixed point iterations for momentum variable
                p0 = pNew;
                oldV = pNew;
                for i = 1:self.FixedPointSteps
                    for d=1:D
                        pTinvGdGinvGp(d) = pNew'*invGdGinvGnew(:,:,d)*pNew;
                    end
                    pNew = p0 - (self.StepSize*0.5)*( -self.Target.LogLikelihoodGradient(xNew) + 0.5*trinvGdGnew' - 0.5*pTinvGdGinvGp' ) ;
                    %check converence and break
                    if norm(oldV./norm(oldV) - pNew./norm(pNew)) < self.cE
                        break;
                    end
                    oldV = pNew;
                end
                
                
                x0 = xNew;
                invG0 = invGnew;
                oldV = xNew;
                % Fixed point iterations for parameters
                for i=1:self.FixedPointSteps
                    xNew = x0 + (self.StepSize*0.5)*( invG0*pNew + invGnew*pNew );
                    % check for overflows and exit to reject
                    if any(isnan(xNew))||any(isinf(xNew)) 
                        warning('RMHMCKernel:IntegrationError','overflow in x');
                        return;
                    end
                    
                    % recalculate metric tensor for the new parameters
                    Gnew = self.Target.MetricTensor(xNew);
                    
                    [cholGnew err] = chol(Gnew);
                    % check for overflows and exit to reject
                    if err~=0 
                        warning('RMHMCKernel:IntegrationError','metric tensor is not positive definite');
                        return;
                    end
                    
                    invGnew = cholGnew \(cholGnew'\eye(D));
                    %RI = solve_tril(cholGnew', IdentityD);
                    %invGnew = solve_triu(cholGnew, RI);
                    
                    %check converence and break
                     if norm(oldV./norm(oldV) - xNew./norm(xNew)) < self.cE
                        break;
                    end
                    oldV = xNew;
                end
                
                % recalculate metric tensor derivatives
                dGnew = self.Target.MetricTensorDerivative(xNew);
                for d = 1:D
                    invGdGinvGnew(:,:,d)     = invGnew*dGnew{d};
                    trinvGdGnew(d)   = trace(invGdGinvGnew(:,:,d));
                    invGdGinvGnew(:,:,d) = invGdGinvGnew(:,:,d) *invGnew;
                end
                
                for d=1:D
                    pTinvGdGinvGp(d) = pNew'*invGdGinvGnew(:,:,d)*pNew;
                end
                pNew = pNew - (self.StepSize*0.5)*( -self.Target.LogLikelihoodGradient(xNew) + 0.5*trinvGdGnew' - 0.5*pTinvGdGinvGp' ) ;
                
                if nargout > 2
                    %save current H;
                    LLnew = self.Target.LogLikelihood(xNew);
                    E(StepNum) = - LLnew + sum(log(diag(cholGnew))) + 0.5*pNew'*invGnew*pNew;
                end
            end
            
            % Calculate proposed H value
            LLnew = self.Target.LogLikelihood(xNew);
            ProposedH = - LLnew + sum(log(diag(cholGnew))) + 0.5*pNew'*invGnew*pNew;
            CurrentH = - self.State_LogPosterior + sum(log(diag(self.cholG))) + 0.5*p'*self.invG*p ;
            
            % Accept according to ratio
            Ratio = -ProposedH + CurrentH;
            
            if Ratio > 0 || (Ratio > log(rand))
                self.State_LogPosterior = LLnew;
                self.State_x = xNew;
                self.G = Gnew;
                self.cholG = cholGnew;
                self.invG  = invGnew;
                self.dG = dGnew;
                self.invGdGinvG = invGdGinvGnew;
                self.trinvGdG  = trinvGdGnew ;
                
                x = xNew;
                
                
                acc = 1;
            end
            
        end
        
        function printStats(self)
            fprintf(1, 'RMHMC Kernel: StepSize = %g, LeapFrogSteps = %u\n',...
                self.StepSize, self.LeapFrogSteps);
            fprintf(1, 'Current LogPosterior = %g\n',...
                self.State_LogPosterior);
        end
        
    end
    
    methods(Static)
        
        %Convinience factory method
        function rmhmc_kernel = NewRMHMCKernel(target,varargin)
            
            if ~isa(target,'TargetDistribution') 
                error('RMHMCKernel:BadConstruction', ...
                    'target should be an instance of TargetDistribution');
           end
            
            argP = inputParser;
            argP.KeepUnmatched = true;
            argP.addParamValue('LeapFrogSteps', 6,  @(x) x > 0 );
            argP.addParamValue('StepSize', 0.15,  @(x) x > 0 );
            argP.addParamValue('FixedPointSteps', 10,  @(x) x > 0 );
            argP.addParamValue('Init', 'model');
            argP.parse( varargin{:} );
            
            
            NumOfLeapFrogSteps = argP.Results.LeapFrogSteps;
            StepSize = argP.Results.StepSize;
            FixedPointSteps = argP.Results.FixedPointSteps;
            Init = argP.Results.Init;
            
            
            if ~(isnumeric(Init) && isvector(Init))
                Init = target.InitParameters;
            end
            
            rmhmc_kernel = RMHMCKernel(target,StepSize,NumOfLeapFrogSteps,FixedPointSteps,Init); 
        end
    end
    
end

