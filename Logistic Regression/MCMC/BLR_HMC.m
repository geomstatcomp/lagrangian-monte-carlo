function [ ] = BLR_hmc( DataSet )

% Random Numbers...
randn('state', 2012);
rand('twister', 2012);

switch(DataSet)

    case 'Australian'
    
        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/australian.mat');
        t=X(:,end);
        X(:,end)=[];
        
        % Normalise Data
        [N, D] = size(X);
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);

        %Create Polynomial Basis
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end
        [N,D] = size(XX);

        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 40;
        StepSize           = 4.5/NumOfLeapFrogSteps;
        Mass               = diag(ones(D,1)*1);

        
    case 'German'

        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/german.mat');
        t=X(:,end);
        X(:,end)=[];

        % Normalise Data
        [N, D] = size(X);
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        % German Credit - replace all 1s in t with 0s
        t(find(t==1)) = 0;
        % German Credit - replace all 2s in t with 1s
        t(find(t==2)) = 1;

        %Create Polynomial Basis
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 64;
        StepSize           = 4/NumOfLeapFrogSteps;
        Mass               = diag(ones(D,1)*1);
        
        
    case 'Heart'
        
        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/heart.mat');
        t=X(:,end);
        X(:,end)=[];

        % Normalise Data
        [N, D] = size(X);
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        % German Credit - replace all 1s in t with 0s
        t(find(t==1)) = 0;
        % German Credit - replace all 2s in t with 1s
        t(find(t==2)) = 1;

        %Create Polynomial Basis
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);

        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 25;
        StepSize           = 4.5/NumOfLeapFrogSteps;
        Mass               = diag(ones(D,1)*1);
        
    case 'Pima'

        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/pima.mat');
        t=X(:,end);
        X(:,end)=[];

        % Normalise Data
        [N, D] = size(X);
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);

        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 50;
        StepSize           = 5/NumOfLeapFrogSteps;
        Mass               = diag(ones(D,1)*1);
        
    case 'Ripley'

        %Two hyperparameters of model
        Polynomial_Order = 3;
        alpha=100;

        %Load and prepare train & test data
        load('./Data/ripley.mat');
        t=X(:,end);
        X(:,end)=[];

        % Normalise Data
        [N, D] = size(X);
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);

        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 23;
        StepSize           = 3.5/NumOfLeapFrogSteps;
        Mass               = diag(ones(D,1)*1);
        
%     case 'CTG'
%     
%         %Two hyperparameters of model
%         Polynomial_Order = 1;
%         alpha=100; 
% 
%         %Load and prepare train & test data
%         %data set is already standardized
%         load('./Data/CTG.mat');
%         t = y;
%         XX = x;
%         [N,D] = size(XX);
% 
%         % HMC Setup
%         NumOfIterations    = 20000;
%         BurnIn             = 5000;
%         NumOfLeapFrogSteps = 40;
%         StepSize           = 0.05;
%         Mass               = diag(ones(D,1)*1);
%         
%         
%     case 'chess'
%     
%         %Two hyperparameters of model
%         Polynomial_Order = 1;
%         alpha=100; 
% 
%         %Load and prepare train & test data
%         %data set is already standardized
%         load('./Data/chess.mat');
%         t = y;
%         XX = x;
%         [N,D] = size(XX);
% 
%         % HMC Setup
%         NumOfIterations    = 20000;
%         BurnIn             = 5000;
%         NumOfLeapFrogSteps = 60;
%         StepSize           = 0.04;
%         Mass               = diag(ones(D,1)*1);
        
end

Proposed = 0;
Accepted = 0;

InvMass = sparse(inv(Mass));

% Set initial values of w
w = ones(D,1)*1e-3;
wSaved = zeros(NumOfIterations-BurnIn,D);

% Calculate joint log likelihood for current w
% LogPrior      = -ecmnobj(w',zeros(D,1),alpha*eye(D));
LogPrior      = -w'*w/2/alpha;
f             = XX*w;
LogLikelihood = f'*t - sum(log(1+exp(f))); %training likelihood
CurrentLJL    = LogLikelihood + LogPrior;


for IterationNum = 1:NumOfIterations
        
    if mod(IterationNum,100) == 0
        disp([num2str(IterationNum) ' iterations completed.'])
        disp(Accepted/Proposed)
        Proposed = 0;
        Accepted = 0;
        drawnow
    end
    
    % Sample momentum
    ProposedMomentum = (randn(1,D)*Mass)';
    OriginalMomentum = ProposedMomentum;
        
    wNew = w;
    Proposed = Proposed + 1;
      
    % random number of leapfrog steps
    if (randn > 0.5) TimeStep = 1; else TimeStep = -1; end
    %RandomStep = ceil(rand*NumOfLeapFrogSteps);
        
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        f = XX*wNew;
        ProposedMomentum = ProposedMomentum + (TimeStep*StepSize/2).*( XX'*( t - (exp(f)./(1+exp(f))) ) - (1/alpha)*wNew );
            
        if sum(isnan(ProposedMomentum)) > 0
            break
        end
        
        wNew = wNew + (TimeStep*StepSize).*(ProposedMomentum);            
            
        f = XX*wNew;
        ProposedMomentum = ProposedMomentum + (TimeStep*StepSize/2).*( XX'*( t - (exp(f)./(1+exp(f))) ) - (1/alpha)*wNew );
            
    end
        
    % Calculate proposed H value
%     LogPrior      = -ecmnobj(wNew',zeros(D,1),alpha*eye(D));
    LogPrior      = -wNew'*wNew/2/alpha;
    f             = XX*wNew;
    LogLikelihood = f'*t - sum(log(1+exp(f))); %training likelihood
    ProposedLJL   = LogLikelihood + LogPrior;
    
    ProposedH = -ProposedLJL + (ProposedMomentum'*InvMass*ProposedMomentum)/2;
        
    % Calculate current H value
    CurrentH  = -CurrentLJL + (OriginalMomentum'*InvMass*OriginalMomentum)/2;
       
    % Accept according to ratio
    Ratio = -ProposedH + CurrentH;
        
        
    if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
        CurrentLJL = ProposedLJL;
        w = wNew;
        Accepted = Accepted + 1;
    end

    % Save samples if required
    if IterationNum > BurnIn
        wSaved(IterationNum-BurnIn,:) = w;
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing posterior samples.')
        tic;
    end
    
end

% Stop timer
TimeTaken = toc;

betaPosterior = wSaved;
acprat = size(unique(betaPosterior,'rows'),1)/(NumOfIterations-BurnIn)

CurTime = fix(clock);
cd('..')
save(['Results/Results_HMC_lr_' DataSet '_' num2str(CurTime) '.mat'], 'StepSize', 'NumOfLeapFrogSteps', 'acprat', 'betaPosterior', 'TimeTaken')


% Plot paths and histograms
%figure(1)
%plot(wSaved);
%figure(2)
%for d = 1:D
%    subplot(ceil(D/4),4,d)
%    hist(wSaved(:,d))
%end


end
