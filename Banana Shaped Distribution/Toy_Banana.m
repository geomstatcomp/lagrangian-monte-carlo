%%% This is to compare the sampling efficiency of HMC, RHMC, sLMC, LMC
%%% in exploring a banana shaped posterior distribution.

function [] = toy_banana(NumOfIterations, BurnIn)

if(nargin==0)
    NumOfIterations    = 25000;
    BurnIn             = 5000;
elseif(nargin==1)
    BurnIn = floor(.2*NumOfIterations);
elseif(nargin>2)
    error('wrong number of inputs!');
end

% Random Numbers...
randn('state', 2012);
rand('twister', 2012);

% simulated data
global N D sigma2y sigma2theta
D=2;
N=100; C=1;
sigma2y=4; sigma2theta=1;
y=C+sqrt(sigma2y).*randn(N,1);


% % draw posterior
% [theta1, theta2] = meshgrid([-2:.05:2],[-2:.05:2]);
% thetaseq = [theta1(:) theta2(:)];
% % thetaseq = [thetaseq(:,1) zeros(size(thetaseq,1),D-2) thetaseq(:,2)];
% potent = reshape(cellfun(@(theta)U(y,theta'), num2cell(thetaseq,2)),size(theta1));
% % normalize the density
% nadir = min(min(potent));
% postdf = exp(-potent+nadir)+10;
% postdf = postdf./sum(sum(postdf));
% 
% fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
% contour(theta1,theta2,postdf);
% title('Posterior Density Surface');
% drawnow;


% settings
Trajectory = 1.2;
NumOfNewtonSteps = 5;

Alg = [0 1 1 0];
NLeap = [9 6 2 2];


%% HMC

if Alg(1)

% sampling setting
NumOfLeapFrogSteps = NLeap(1);
StepSize = Trajectory/NumOfLeapFrogSteps;

thetaSaved = zeros(NumOfIterations-BurnIn,D);
% Initialize
theta = -ones(D,1); theta(2:2:D) = 1.5;
% Calculate potential for current theta
CurrentU  = U(y,theta);
dU = U(y,theta,1);

accepted = 0;
disp(' ');
disp('Running HMC...');
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    thetaNew = theta;
    dUNew = dU;
    
    % propose velocity
    Velocity = randn(D,1);
    
    % Calculate current H value
    CurrentH  = CurrentU + sum(Velocity.^2)/2;
    
%     randL = ceil(rand*NumOfLeapFrogSteps);
    
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        Velocity = Velocity- (StepSize/2)*dUNew;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        % Make a full step for the position
        thetaNew = thetaNew + StepSize*Velocity;
        
        
        % terms other than quadratic one
        dUNew = U(y,thetaNew,1);
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        Velocity = Velocity- (StepSize/2)*dUNew;
        
    end
    
    % Calculate proposed H value
    ProposedU = U(y,thetaNew);
    ProposedH = ProposedU + sum(Velocity.^2)/2;
    
    
    % Accept according to ratio
    Ratio = -ProposedH + CurrentH;
    
    if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
        theta= thetaNew; dU = dUNew;
        CurrentU = ProposedU;
        acceptedyes = 1;
    else
        acceptedyes = 0;
    end
    
    % Save samples if required
    if IterationNum > BurnIn
        thetaSaved(IterationNum-BurnIn,:) = theta';
        accepted = accepted + acceptedyes;
        if mod(IterationNum,100) == 0
            disp([num2str(IterationNum) ' iterations completed.'])
            disp(accepted/(IterationNum-BurnIn));
        end
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing samples.')
        tic;
    end
        
end

TimeTaken=toc;
CurTime = fix(clock);
acpt = accepted/(NumOfIterations-BurnIn);
save(['Results_toy/Banana_hmc_D',num2str(D),'_L',num2str(NumOfLeapFrogSteps),'_' num2str(CurTime),'.mat'],'thetaSaved','StepSize','NumOfLeapFrogSteps','acpt','TimeTaken')
disp(' ');
disp(['Accpetance Rate of HMC: ',num2str(acpt)]);
disp(' ');
currentfolder=cd('./Results_toy/');
CalculateStatistics('Banana_hmc');
cd(currentfolder);

end

warning('off');
%% RHMC

if Alg(2)

% sampling setting
NumOfLeapFrogSteps = NLeap(2);
StepSize = Trajectory/NumOfLeapFrogSteps;

thetaSaved = zeros(NumOfIterations-BurnIn,D);
% Initialize
theta = -ones(D,1); theta(2:2:D) = 1.5;
% Calculate potential energy for current theta
CurrentU  = U(y,theta);
% Calculate G
[G InvG dG] = Met(theta,[0 -1 1]);
CholG = chol(G);
% Calculate the partial derivatives dG/dw
for d = 1:D
    TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
end
% terms other than quadratic one
dphi = U(y,theta,1) + 0.5*TraceInvGdG';

accepted = 0;
disp(' ');
disp('Running RHMC...');
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    thetaNew = theta;
    CholGNew=CholG; InvGNew=InvG; dGNew=dG;
    dphiNew=dphi;
    
    % propose momentum
    Momentum = (randn(1,D)*CholGNew)';
    
    % Calculate current H value
    CurrentLogDet = sum(log(diag(CholGNew)));
    CurrentH  = CurrentU + CurrentLogDet + (Momentum'*InvGNew*Momentum)/2;
    
%     randL = ceil(rand*NumOfLeapFrogSteps);
    
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        % Multiple fixed point iteration
        PM = Momentum;
        for FixedIter = 1:NumOfNewtonSteps
            MomentumHist(FixedIter,:) = PM;
            
            InvGMomentum = InvGNew*PM;
            for d = 1:D
                dQuadTerm(d)  = 0.5*(InvGMomentum'*dGNew(:,:,d)*InvGMomentum);
            end
            
            PM = Momentum + (StepSize/2)*(-dphiNew + dQuadTerm');
        end
        Momentum = PM;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        %%% Multiple Fixed Point Iteration %%%
        Ptheta = thetaNew;
        for FixedIter = 1:NumOfNewtonSteps
            thetaHist(FixedIter,:) = Ptheta;
            
            InvGMomentum = InvGNew*Momentum;
            if FixedIter==1
                FixedInvGMomentum = InvGMomentum;
            end
            Ptheta = thetaNew + (StepSize/2)*(FixedInvGMomentum + InvGMomentum);
            if FixedIter~=NumOfNewtonSteps
                [~,InvGNew] = Met(Ptheta,[0 -1]);
            else
                [G InvGNew dGNew] = Met(Ptheta,[0 -1 1]);
            end
            
        end
        thetaNew = Ptheta;
        
        
        % Update G based on new parameters
        % Update the partial derivatives dG/dw
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvGNew.*dGNew(:,:,d)'));
        end
        % terms other than quadratic one
        dphiNew = U(y,thetaNew,1) + 0.5*TraceInvGdG';
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        InvGMomentum = InvGNew*Momentum;
        for d = 1:D
            dQuadTerm(d) = 0.5*(InvGMomentum'*dGNew(:,:,d)*InvGMomentum);
        end
        Momentum = Momentum + (StepSize/2)*(-dphiNew + dQuadTerm');
        
    end
    
    try
        
        % Calculate proposed H value
        ProposedU = U(y,thetaNew);
        CholGNew = chol(G);
        ProposedH = ProposedU + sum(log(diag(CholGNew))) + (Momentum'*InvGNew*Momentum)/2;


        % Accept according to ratio
        Ratio = -ProposedH + CurrentH;

        if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
            theta= thetaNew; CurrentU = ProposedU;
            CholG=CholGNew; InvG=InvGNew; dG=dGNew;
            dphi=dphiNew;
            acceptedyes = 1;
        else
            acceptedyes = 0;
        end
    catch
        acceptedyes = 0;
%         disp('Break!');
    end
    
    % Save samples if required
    if IterationNum > BurnIn
        thetaSaved(IterationNum-BurnIn,:) = theta';
        accepted = accepted + acceptedyes;
        if mod(IterationNum,100) == 0
            disp([num2str(IterationNum) ' iterations completed.'])
            disp(accepted/(IterationNum-BurnIn));
        end
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing samples.')
        tic;
    end
    
end

TimeTaken=toc;
CurTime = fix(clock);
acpt = accepted/(NumOfIterations-BurnIn);
save(['Results_toy/Banana_rhmc_D',num2str(D),'_L',num2str(NumOfLeapFrogSteps),'_' num2str(CurTime),'.mat'],'thetaSaved','StepSize','NumOfLeapFrogSteps','NumOfNewtonSteps','acpt','TimeTaken')
disp(' ');
disp(['Accpetance Rate of RHMC: ',num2str(acpt)]);
disp(' ');
currentfolder=cd('./Results_toy/');
CalculateStatistics('Banana_rhmc');
cd(currentfolder);

end


%% sLMC

if Alg(3)

% sampling setting
NumOfLeapFrogSteps = NLeap(3);
StepSize = Trajectory/NumOfLeapFrogSteps;

thetaSaved = zeros(NumOfIterations-BurnIn,D);
% Initialize
theta = -ones(D,1); theta(2:2:D) = 1.5;
% Calculate potential energy for current theta
CurrentU  = U(y,theta);
% Calculate G
[G InvG dG Gamma1] = Met(theta,[0 -1 1 3]);
CholInvG = chol(InvG);
% Calculate the partial derivatives dG/dq
for d = 1:D
    TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
end
% terms other than quadratic one
dphi = U(y,theta,1) + 0.5*TraceInvGdG';

accepted = 0;
disp(' ');
disp('Running sLMC...');
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    thetaNew = theta;
    GNew=G; InvGNew=InvG; CholInvGNew=CholInvG; Gamma1New=Gamma1;
    dphiNew=dphi;
    
    % propose velocity
    Velocity = (randn(1,D)*CholInvGNew)';
    
    % Calculate current H value
    CurrentLogDet = - sum(log(diag(CholInvGNew)));
    CurrentH  = CurrentU + CurrentLogDet + (Velocity'*GNew*Velocity)/2;
    
    % Accumulate determinant to be adjusted in acceptance rate
    Deltalogdet = 0;
    
    randL = ceil(rand*NumOfLeapFrogSteps);
    
    % Perform leapfrog steps
    for StepNum = 1:randL
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Multiple fixed point iteration
        PV = Velocity; dQuadTerm = zeros(D,1);
        for FixedIter = 1:NumOfNewtonSteps
            VelocityHist(FixedIter,:) = PV;
            
            for d = 1:D
                dQuadTerm(d) = PV'*Gamma1New(:,:,d)*PV;
            end
            PV = Velocity - (StepSize/2)*(InvGNew*(dphiNew + dQuadTerm));
        end
        Velocity = PV;
        
        % Record Delta log-determinant
        VGamma1 = zeros(D);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(GNew+StepSize*VGamma1));

        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        thetaNew = thetaNew + StepSize*Velocity;
        
        % Update G based on new parameters
        [GNew InvGNew dG Gamma1New] = Met(thetaNew,[0 -1 1 3]);
        % Update the partial derivatives dG/dq
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvGNew.*dG(:,:,d)'));
        end
        % terms other than quadratic one
        dphiNew = U(y,thetaNew,1) + 0.5*TraceInvGdG';
        
        % Record Delta log-determinant
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(GNew-StepSize*VGamma1));

        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
%         for d = 1:D
%             dQuadTerm(d) = Velocity'*Gamma1(:,:,d)*Velocity;
%         end
        dQuadTerm = VGamma1*Velocity;
        Velocity = Velocity - (StepSize/2)*(InvGNew*(dphiNew + dQuadTerm));
        
    end
    
    try
        % Calculate proposed H value
        ProposedU = U(y,thetaNew);
        CholInvGNew = chol(InvGNew);
        ProposedH = ProposedU - sum(log(diag(CholInvGNew))) + (Velocity'*GNew*Velocity)/2;
        

        % Accept according to ratio
        Ratio = -ProposedH + CurrentH + Deltalogdet;

        if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
            theta= thetaNew; CurrentU = ProposedU;
            G=GNew; InvG=InvGNew; CholInvG=CholInvGNew; Gamma1=Gamma1New;
            dphi=dphiNew;
            acceptedyes = 1;
        else
            acceptedyes = 0;
        end
    catch
        acceptedyes = 0;
%         disp('Break!');
    end
    
    % Save samples if required
    if IterationNum > BurnIn
        thetaSaved(IterationNum-BurnIn,:) = theta';
        accepted = accepted + acceptedyes;
        if mod(IterationNum,100) == 0
            disp([num2str(IterationNum) ' iterations completed.'])
            disp(accepted/(IterationNum-BurnIn));
        end
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing samples.')
        tic;
    end
    
end

TimeTaken=toc;
CurTime = fix(clock);
acpt = accepted/(NumOfIterations-BurnIn);
save(['Results_toy/Banana_slmc_D',num2str(D),'_L',num2str(NumOfLeapFrogSteps),'_' num2str(CurTime),'.mat'],'thetaSaved','StepSize','NumOfLeapFrogSteps','NumOfNewtonSteps','acpt','TimeTaken')
disp(' ');
disp(['Accpetance Rate of sLMC: ',num2str(acpt)]);
disp(' ');
currentfolder=cd('./Results_toy/');
CalculateStatistics('Banana_slmc');
cd(currentfolder);

end


%% LMC

if Alg(4)

% sampling settings
NumOfLeapFrogSteps = NLeap(4);
StepSize = Trajectory/NumOfLeapFrogSteps;

thetaSaved = zeros(NumOfIterations-BurnIn,D);
% Initialize
theta = -ones(D,1); theta(2:2:D) = 1.5;
% Calculate potential energy for current theta
CurrentU  = U(y,theta);
% Calculate G
[G InvG dG Gamma1] = Met(theta,[0 -1 1 3]);
CholInvG = chol(InvG);
% Calculate the partial derivatives dG/dq
for d = 1:D
    TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
end
% terms other than quadratic one
dphi = U(y,theta,1)+ 0.5*TraceInvGdG';

accepted = 0;
disp(' ');
disp('Running LMC...');
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    thetaNew = theta;
    GNew=G; CholInvGNew=CholInvG; Gamma1New=Gamma1;
    dphiNew=dphi;
    
    % propose velocity
    Velocity = (randn(1,D)*CholInvGNew)';
    
    % Calculate current H value
    CurrentLogDet = sum(log(diag(CholInvGNew))); % adjusted
    CurrentH = CurrentU + CurrentLogDet + (Velocity'*GNew*Velocity)/2;
    
    % Accumulate determinant to be adjusted in acceptance rate
    Deltalogdet = 0;
    
    randL = ceil(rand*NumOfLeapFrogSteps);
    
    
    % Perform leapfrog steps
    for StepNum = 1:randL
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        VGamma1 = zeros(D);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(GNew+(StepSize/2)*VGamma1));
        Velocity = (GNew+(StepSize/2)*VGamma1)\(GNew*Velocity- (StepSize/2)*dphiNew);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(GNew-(StepSize/2)*VGamma1));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        % Make a full step for the position
        thetaNew = thetaNew + StepSize*Velocity;
        
        
        % Update G based on new parameters
        [GNew InvG dG Gamma1New] = Met(thetaNew,[0 -1 1 3]);
        % Update the partial derivatives dG/dq
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
        end
        % terms other than quadratic one
        dphiNew = U(y,thetaNew,1)+ 0.5*TraceInvGdG';
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(GNew+(StepSize/2)*VGamma1));
        Velocity = (GNew+(StepSize/2)*VGamma1)\(GNew*Velocity- (StepSize/2)*dphiNew);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(GNew-(StepSize/2)*VGamma1));
        
    end
    
    try
        
        % Calculate proposed H value
        ProposedU = U(y,thetaNew);
        CholInvGNew = chol(InvG);
        ProposedH = ProposedU + sum(log(diag(CholInvGNew))) + (Velocity'*GNew*Velocity)/2;


        % Accept according to ratio
        Ratio = -ProposedH + CurrentH + Deltalogdet;

        if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
            theta= thetaNew; CurrentU = ProposedU;
            G=GNew; Gamma1=Gamma1New; CholInvG=CholInvGNew;
            dphi=dphiNew;
            acceptedyes = 1;
        else
            acceptedyes = 0;
        end
    catch
        acceptedyes = 0;
%         disp('Break!');
    end
    
    % Save samples if required
    if IterationNum > BurnIn
        thetaSaved(IterationNum-BurnIn,:) = theta';
        accepted = accepted + acceptedyes;
        if mod(IterationNum,100) == 0
            disp([num2str(IterationNum) ' iterations completed.'])
            disp(accepted/(IterationNum-BurnIn));
        end
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing samples.')
        tic;
    end
    
end

TimeTaken=toc;
CurTime = fix(clock);
acpt = accepted/(NumOfIterations-BurnIn);
save(['Results_toy/Banana_lmc_D',num2str(D),'_L',num2str(NumOfLeapFrogSteps),'_' num2str(CurTime),'.mat'],'thetaSaved','StepSize','NumOfLeapFrogSteps','acpt','TimeTaken')
disp(' ');
disp(['Accpetance Rate of LMC: ',num2str(acpt)]);
disp(' ');
currentfolder=cd('./Results_toy/');
CalculateStatistics('Banana_lmc');
cd(currentfolder);

end



end



%% functions needed
function [output] = U(y,theta,der)
if(nargin<3)
    der=0;
end

global N D sigma2y sigma2theta

mu=sum([theta(1:2:D);theta(2:2:D).^2]);

if der==0
    logpri = -theta'*theta/2/sigma2theta;
    loglik = -(y-mu)'*(y-mu)/2/sigma2y;
    output = -(loglik+logpri);
elseif der==1
    dlogpri = -theta./sigma2theta;
    theta_tmp = ones(D,1); theta_tmp(2:2:D) = 2.*theta(2:2:D);
    dloglik = (sum(y)-N*mu)/sigma2y.*theta_tmp;
    output  = -(dloglik + dlogpri);
else
    disp('wrong choice of der!')
end

end


function [G InvG dG Gamma1] = Met(theta,opt)
if(nargin<2)
    opt=0;
end

global N D sigma2y sigma2theta

theta_tmp = ones(D,1); theta_tmp(2:2:D) = 2.*theta(2:2:D);

G = N/sigma2y.*(theta_tmp*theta_tmp');
G(1:D+1:D^2) = G(1:D+1:D^2) + 1/sigma2theta;
if all(opt==0)
    InvG=NaN; dG=NaN; Gamma1=NaN;
else
    if any(opt==-1)
%         InvG = inv(G);
        InvG = sigma2theta.*(eye(D)-(theta_tmp*theta_tmp')./(sigma2y/N/sigma2theta+theta_tmp'*theta_tmp));
    end
    if any(opt==1)
        dG = zeros(repmat(D,1,3));
        for k=2:2:D
            dG(k,:,k) = N/sigma2y.*(2*theta_tmp'); dG(:,k,k) = dG(:,k,k) + N/sigma2y.*(2*theta_tmp);
        end
        if any(opt==3)
            Gamma1 = .5*(permute(dG,[1,3,2]) + permute(dG,[3,2,1]) - dG);
        else
            Gamma1=NaN;
        end
    else
        dG=NaN; Gamma1=NaN;
    end
end

end

