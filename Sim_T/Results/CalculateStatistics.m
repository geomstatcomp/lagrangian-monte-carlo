function [MinESSs, MinESS, MeanESS, MedianESS, MaxESS, MinACT, MedianACT, MaxACT, MeanACT, StepSize,NumOfLeapFrogSteps, ACC,Times] = CalculateStatistics( Method, dim )

% Get stats
Files = dir(['*' Method '*' dim '*.mat']);


for i = 1:length(Files)

    Data     = open(Files(i).name);
    
    ESS{i}   = CalculateESS(Data.xSaved, length(Data.xSaved)-1);
    
    MinESS(i)    = min(ESS{i});
    MaxESS(i)    = max(ESS{i});
    MedianESS(i) = median(ESS{i});
    MeanESS(i)   = mean(ESS{i});
    Times(i)     = Data.TimeTaken;
    StepSize(i)  = Data.StepSize;
    NumOfLeapFrogSteps(i) = Data.NumOfLeapFrogSteps;
    ACC(i)       = Data.acpt;
    
    ACT{i}   = CalculateACT(Data.xSaved);
    MinACT(i)    = min(ACT{i});
    MaxACT(i)    = max(ACT{i});
    MedianACT(i) = median(ACT{i});
    MeanACT(i)   = mean(ACT{i});
    
end

% disp(['Time:   ' num2str(mean(Times)) ' +/- ' num2str(std(Times)/sqrt(length(Times)))])
% disp(' ')

disp(['Time per iteration:   ' num2str(mean(Times)/size(Data.xSaved,1))])
disp(' ')

disp(['ESS for ' Method ' for simulation.'])
disp(' ')

disp(['Min:    ' num2str(mean(MinESS)) ' +/- ' num2str(std(MinESS)/sqrt(length(MinESS)))])
disp(['Median: ' num2str(mean(MedianESS)) ' +/- ' num2str(std(MedianESS)/sqrt(length(MedianESS)))])
disp(['Mean:   ' num2str(mean(MeanESS)) ' +/- ' num2str(std(MeanESS)/sqrt(length(MeanESS)))])
disp(['Max:    ' num2str(mean(MaxESS)) ' +/- ' num2str(std(MaxESS)/sqrt(length(MaxESS)))])

disp('')
MinESSs=mean(MinESS)/mean(Times);
disp(['Min ESS per second: ' num2str(MinESSs)])
disp(' ')


disp(['ACT for ' Method ' for simulation.'])
disp(' ')

disp(['Min:    ' num2str(mean(MinACT)) ' +/- ' num2str(std(MinACT)/sqrt(length(MinACT)))])
disp(['Median: ' num2str(mean(MedianACT)) ' +/- ' num2str(std(MedianACT)/sqrt(length(MedianACT)))])
disp(['Mean:   ' num2str(mean(MeanACT)) ' +/- ' num2str(std(MeanACT)/sqrt(length(MeanACT)))])
disp(['Max:    ' num2str(mean(MaxACT)) ' +/- ' num2str(std(MaxACT)/sqrt(length(MaxACT)))])

disp('')
disp(['Time multiply Mean ACT: ' num2str(mean(Times)*mean(MeanACT))])

end
