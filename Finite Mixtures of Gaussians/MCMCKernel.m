classdef MCMCKernel < handle
    %MCMCKERNEL Abstract class (intefrace) for MCMC kernels.
    %   Vassilios Stathopoulos, stathv@gmail.com
    %   September 2012.
    
    properties (Abstract = true)
        State_x;
        State_LogPosterior;
        Target;
        NumberOfParameters;
    end
    
    
    methods (Abstract = true)
        [x acc] = nextSample(self);
        paramsStruct = getParams(self);
    end
    
    methods 

        function a = printStats(self)
            error('TargetDistribution:NoGradientInformation','Metric Tensor Not Implemented');
        end
        
    end
end

