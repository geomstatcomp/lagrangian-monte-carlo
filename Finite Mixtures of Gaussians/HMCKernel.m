classdef HMCKernel < MCMCKernel & HMCAdaptiveKernel
    % HMCKernel Hamiltonian Monte Carlo kernel
    %   Vassilios Stathopoulos, stathv@gmail.com
    %   September 2012
    %
    %  Constructors:
    %       hmc_kernel = NewHMCKernel(target)
    %           Constructs a new HMCKernel with step-size 0.15, 6
    %           leap-frog integration steps and arbitary initial values.
    %           target    : Target density, an object implementing 
    %                       the TargetDistribution interface.
    %
    %       hmc_kernel = NewHMCKernel(target, PropertyName,Value, ... )
    %           Construct a new HMCKernel with properties spesified as
    %           parameters. Valid property names are:
    %           'StepSize'         : Integration step size.
    %           'LeapFrogSteps'    : Number of leap-frog integration steps.
    %           'Init'             : Initial values for the chain.
    %           'MassMatrix'       : Mass matrix for the proposal.
    %       Example: 
    %           hmc_kernel = NewHMCKernel(myTarget, 'StepSize',0.5);
    %           
    %       NewHMCKernel(Target,StepSize,LeapFrogSteps,Init,MassMatrix)
    %           Target                : The target density.
    %           StepSize              : Integration step size.
    %           LeapFrogStep          : Number of leap-frog integration 
    %                                   steps.
    %           Init                  : Initial values for the chain.
    %           MassMatrix            : Mass matrix for the proposal.
    %
    %  Properties:
    %       State_x              : Current value of the chain.
    %       State_LogPosterior   : Value of the target at State_x.
    %       Target               : Target density.
    %       NumberOfParameters   : Length of State_x.
    %       TargetAccRate        : Optimal acceptance rate. Used only for
    %                              tunning.
    %       MassMatrix           : Mass matrix for the proposal.
    %
    %
    %   Methods:
    %       [x acc] = nextSample()
    %           Returns a new random sample from the target density.
    %           x:      The new sample.
    %           acc:    1 if x is a new sample (accepted proposal) and 0 if
    %                   x is the same as the previous sample in the chain
    %                   (rejected proposal).
    %
    %       printStats()
    %           Prints kernel statistics, step-size, number of leap-frog
    %           integration steps and current value of the target density on
    %           the console.
    %
    
    properties
        State_x;
        State_LogPosterior;
        Target;
        MassMatrix;
    end
    
    properties (Dependent)
        NumberOfParameters;
    end
    
    properties (Constant)
        TargetAccRate = 0.75;
    end
    
    properties (Access = private);
        cholL;
        invM;
    end
    
    methods (Static, Access=private)
        
        % remove??
        
    end
    
    methods
        function self = HMCKernel(Target,StepSize,LeapFrogSteps,Init,MassMatrix)
            if ~isa(Target,'TargetDistribution')
                error('HMCKernel:BadConstruction', ...
                    'target should be an instance of TargetDistribution');
            end
            
            if length(Target.InitParameters)~=length(Init)
                error('HMCKernel:BadConstruction', ...
                    'Init must have the same size as Target.InitParamaters');
            end
            
            self.Target = Target;
            self.State_x = Init;
            
            self.StepSize = StepSize;
            self.LeapFrogSteps = LeapFrogSteps;
            self.MassMatrix = MassMatrix;
            self.cholL = chol(MassMatrix);
            self.invM  = inv(MassMatrix);
            
            % initial condition
            D = self.NumberOfParameters;
            self.State_LogPosterior = Target.LogLikelihood(self.State_x);
            
            
        end
        
        function NumberOfParameters = get.NumberOfParameters(self)
            NumberOfParameters = length(self.State_x);
        end
        
        

        function [x acc] = nextSample(self)
            
            acc = 0;
            x = self.State_x;
            D = self.NumberOfParameters;
            
            % sample momenta
            p = ( randn(1, D) * self.cholL )';
            pNew = p;
            xNew = self.State_x;
            
            % Randomise number of leap-frog steps.
            RandomSteps = ceil(rand*self.LeapFrogSteps);
            if nargout > 2
                RandomSteps = self.LeapFrogSteps;
            end
            
            
            for StepNum = 1:RandomSteps
               pNew = pNew + (self.StepSize*0.5)*( self.Target.LogLikelihoodGradient(xNew) );
               xNew = xNew + (self.StepSize)*self.invM*pNew;
               pNew = pNew + (self.StepSize*0.5)*( self.Target.LogLikelihoodGradient(xNew) );
                
            end
                        
            % Calculate proposed H value
            LLnew = self.Target.LogLikelihood(xNew);
           
            ProposedH = - LLnew + 0.5*pNew'*self.invM*pNew;
            CurrentH = - self.State_LogPosterior + 0.5*p'*self.invM*p;
            
            % Accept according to ratio
            Ratio = -ProposedH + CurrentH;
            
            if (isfinite(Ratio) && (Ratio > min([0,log(rand)])))
            %if Ratio > 0 || (Ratio > log(rand))
                self.State_LogPosterior = LLnew;
                self.State_x = xNew;
                
                x = xNew;
                acc = 1;
            end
            
        end
        
        function printStats(self)
            fprintf(1, 'HMCKernel Kernel: StepSize = %g, LeapFrogSteps = %u\n',...
                self.StepSize, self.LeapFrogSteps);
            fprintf(1, 'Current LogPosterior = %g\n',...
                self.State_LogPosterior);
        end
        
    end
    
    methods(Static)
        
        %Convinience factory method
        function hmc_kernel = NewHMCKernel(target,varargin)
            
            if ~isa(target,'TargetDistribution')
                error('HMCKernel:BadConstruction', ...
                    'target should be an instance of TargetDistribution');
            end
            
            argP = inputParser;
            argP.KeepUnmatched = true;
            argP.addParamValue('LeapFrogSteps', 6,  @(x) x > 0 );
            argP.addParamValue('StepSize', 0.15,  @(x) x > 0 );
            argP.addParamValue('Init', 'model');
            argP.addParamValue('MassMatrix', 'eye', @(x) ismatrix(x) );
            argP.parse( varargin{:} );
            
            
            NumOfLeapFrogSteps = argP.Results.LeapFrogSteps;
            StepSize = argP.Results.StepSize;
            Init = argP.Results.Init;
            MassMatrix = argP.Results.MassMatrix;
            
            
            if ~(isnumeric(Init) && isvector(Init))
                Init = target.InitParameters;
            end
            
            if ~(isnumeric(MassMatrix) && ismatrix(MassMatrix))
                MassMatrix = sparse(eye(length(Init)));
            else
                [mr, mc] = size(MassMatrix);
                [L, err] = chol(MassMatrix);
                
                if mr ~= length(Init) || mc ~= length(Init) || err ~= 0
                     error('HMCKernel:BadConstruction', ...
                    'Mass matrix should be a positive definite matrix of size DxD where D is the number of parameters.');
                end
            end
            
            hmc_kernel = HMCKernel(target,StepSize,NumOfLeapFrogSteps,Init,MassMatrix);
        end
    end
    
end


