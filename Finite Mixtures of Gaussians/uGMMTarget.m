classdef uGMMTarget < TargetDistribution
    %UGMMTARGET Univariate finite mixtures of Gaussians target density.
    %   Vassilios Stathopoulos, stathv@gmail.com
    %   September 2012.
    %   
    %   gmmTargetDensity = uGMMTarget(X, K, dp, mp, Sp, vp1, vp2)
    %   Creates a new GMM target density.
    %   X   : a Nx1 matrix of data.
    %   K   : Number of Gaussian components.
    %   dp  : Hyper-parameter of the symetric Dirichelt prior for the
    %         mixing coefficients.
    %   mp  : Mean hyper-parameter for the Normal prior over the means.
    %   Sp  : Variance hyper-parameter for the Normal prior over the means.
    %   vp1 : Shape hyper-parameter for the Gamma prior over the variances.
    %   vp2 : Scale hyper-parameter for the Gamma prior over the variances.
    %
    
    properties
        X;
        K;
        dp;
        mp;
        Sp;
        vp1;
        vp2;
    end
    
    properties (Access = private)
        N;
    end
    
    
    properties (Dependent)
        InitParameters;
    end
    
    methods (Access = private)
        
        function [norms] = get_normTrms(self,m,v)
            norms = zeros(self.N,self.K);
            for k = 1:self.K
                norms(:,k) = exp(uGMMTarget.log_norm_pdf(self.X, m(k), v(k)));
            end
        end
        
        function [p m v a g]  = tParams(self, x)
            %Transform Parameters
            a = [x(1:self.K-1); 0];
            tmp = exp(a);
            p = tmp./sum(tmp);
            x(1:self.K-1) = [];
            m = x(1:self.K);
            x(1:self.K) =[];
            g = x(1:self.K);
            v = exp(x(1:self.K));
        end
        
    end
    
    methods (Static, Access = private)
      
        function l = log_norm_pdf(x,m,v)
            l = -0.5*log(2*pi.*v) - ((x-m).^2)./(2*v);
        end
        
        function l = log_inv_gamma_pdf(x,a,b)
            l = -gammaln(a) + a.*log(b) - (a+1).*log(x) - b./x;
        end
        
        function l = log_gamma_pdf(x,a,b)
            l = -gammaln(a) - a.*log(b) + (a-1).*log(x) - x./b;
        end
        
    end
    
    methods
        % Constructor
        function self = uGMMTarget(X, K, dp, mp, Sp, vp1, vp2)
            self.X = X;
            self.N = length(X);
            self.K = K;
            self.dp = dp;
            self.mp = mp;
            self.Sp = Sp;
            self.vp1 = vp1;
            self.vp2 = vp2;
            
        end
        
        % Log-likelihood function
        function JLL = LogLikelihood(self, paramsN)
            sP = size(paramsN,2);
            JLL = zeros(sP,1);
            
            for pn = 1:sP
                x = paramsN(:,pn);
                % Joint Log Likelihood
                
                %complexity O( D + N*K )
                [p m v a g]  = self.tParams(x);
                
                %prior
                lDirP = sum(uGMMTarget.log_gamma_pdf(exp(a),self.dp,1) + a);
                lGauP = sum(uGMMTarget.log_norm_pdf(m,self.mp,self.Sp));
                lIGaP = sum(uGMMTarget.log_inv_gamma_pdf(v,self.vp1,self.vp2) + g);
                
                %likelihood
                norms = self.get_normTrms(m,v);
                c = sum(bsxfun(@(x,y) y.*x, norms,p'),2);
                
                %c = p(1)*exp(log_norm_pdf(X,m(1),v(1)));
                %for k=2:K
                %    c = c + p(k)*exp(log_norm_pdf(X,m(k),v(k)));
                %end
                JLL(pn) = sum(log(c)) + lDirP + lGauP + lIGaP;
            end
        end
        
        % Gradient of log-likelihood
        function dJLL = LogLikelihoodGradient(self, paramsN)
            [D sP]  = size(paramsN);
            dJLL = zeros(D,sP);
            for pn = 1:sP
                x = paramsN(:,pn);
                
                % Gradient of the Joint Log Likelihood
                %complexity O( D + N*K )
                [p m v a g]  = self.tParams(x);
                
                %prior
                lDirP = (self.dp-1) - exp(a) + 1;
                lGauP = -(m-self.mp)./self.Sp;
                lIGaP = -(self.vp1+1) + self.vp2./v + 1;
                dPr = [lDirP(1:self.K-1); lGauP; lIGaP];
                
                %likelihood
                norms = self.get_normTrms(m,v);
                rho = bsxfun(@(x,y) y.*x, norms,p');
                rho = bsxfun(@(x,y) x./y, rho, sum(rho,2));
                %for k = 1:K
                %    rho(:,k) = p(k)*exp(log_norm_pdf(X,m(k),v(k)));
                %end
                %rho = bsxfun(@(x,y) x./y, rho, sum(rho,2));
                
                pi_scores = bsxfun(@(x,y) x-y, rho, p');
                
                diff = bsxfun(@(x,y) x-y, repmat(self.X,1,self.K), m');
                diffnorm = bsxfun(@(x,y) x./y, diff, v');
                m_scores = rho.*diffnorm;
                
                diff2norm = bsxfun(@(x,y) x./y, diff.^2, 2*v');
                v_scores = rho.*(diff2norm - 0.5);
                
                
                dl = [sum(pi_scores(:,1:self.K-1))'; sum(m_scores)'; sum(v_scores)'];
                dl = dl + dPr;
                dJLL(:,pn) = dl;
            end
            
        end
        
        function M = MetricTensor(self, x)
            %Empirical Fisher Information Matrix
            %complexity O( D + N*K + D^2*N )
            [p m v a g]  = self.tParams(x);
            
            %prior
            lDirP = - exp(a);
            lGauP = - ones(self.K,1)./self.Sp;
            lIGaP = - self.vp2./v ;
            dPr = [lDirP(1:self.K-1); lGauP; lIGaP];
            
            %likelihood
            norms = self.get_normTrms(m,v);
            rho = bsxfun(@(x,y) y.*x, norms,p');
            rho = bsxfun(@(x,y) x./y, rho, sum(rho,2));
            %for k = 1:K
            %    rho(:,k) = p(k)*exp(log_norm_pdf(X,m(k),v(k)));
            %end
            %rho = bsxfun(@(x,y) x./y, rho, sum(rho,2));
            
            pi_scores = bsxfun(@(x,y) x-y, rho, p');
            
            diff = bsxfun(@(x,y) x-y, repmat(self.X,1,self.K), m');
            diffnorm = bsxfun(@(x,y) x./y, diff, v');
            m_scores = rho.*diffnorm;
            
            diff2norm = bsxfun(@(x,y) x./y, diff.^2, 2*v');
            v_scores = rho.*(diff2norm - 0.5);
            
            S = [pi_scores(:,1:self.K-1) m_scores v_scores];
            s = sum(S);
            M = S'*S - (s'*s)./self.N - diag(dPr);
            
        end
        
        function dM = MetricTensorDerivative(self, x)
            %Partial derivatives of the Empirical Fisher Information Matrix
            %complexity O( D + N*K + D^2*N*K + K^2*N)
            [p m v a g]  = self.tParams(x);
            
            %prior
            lDirP = - exp(a);
            lGauP = 0;
            lIGaP = + self.vp2./v ;
            dPr = [lDirP(1:self.K-1); lGauP; lIGaP];
            
            rho = zeros(self.N,self.K);
            %likelihood
            for k = 1:self.K
                rho(:,k) = p(k)*exp(uGMMTarget.log_norm_pdf(self.X,m(k),v(k)));
            end
            rho = bsxfun(@(x,y) x./y, rho, sum(rho,2));
            pi_scores = bsxfun(@(x,y) x-y, rho, p');
            
            diff = bsxfun(@(x,y) x-y, repmat(self.X,1,self.K), m');
            diffnorm = bsxfun(@(x,y) x./y, diff, v');
            m_scores = rho.*diffnorm;
            
            diff2norm = bsxfun(@(x,y) x./y, diff.^2, 2*v');
            v_scores = rho.*(diff2norm - 0.5);
            
            S = [pi_scores(:,1:self.K-1) m_scores v_scores];
            s = sum(S);
            
            %preallocate for speed
            dGa = cell(self.K,1);
            dGm = cell(self.K,1);
            dGg = cell(self.K,1);
            
            for k = 1:self.K
                rho2_k = - bsxfun(@(x,y) x.*y, rho, rho(:,k));
                % w.r.t. alpha
                daaS = bsxfun(@(x,y) x+y, rho2_k, p'*p(k));
                daaS(:,k) = daaS(:,k) + rho(:,k)-p(k);
                
                damS = rho2_k;
                damS(:,k) = damS(:,k) + rho(:,k);
                damS = damS.*diffnorm;
                
                dagS = rho2_k;
                dagS(:,k) = dagS(:,k) + rho(:,k);
                dagS = dagS.*(diff2norm-0.5);
                
                %dSa_k and dGa_k
                dSa_k = [daaS(:,1:self.K-1) damS dagS];
                dsa_k = sum(dSa_k);
                dGa{k} = dSa_k'*S + S'*dSa_k - (dsa_k'*s+s'*dsa_k)./self.N ;
                
                %a_k prior
                if k < self.K
                    dGa{k}(k,k) = dGa{k}(k,k) + exp(a(k));
                end
                
                %w.r.t. mu
                dmaS = rho2_k;
                dmaS(:,k) = dmaS(:,k) + rho(:,k);
                dmaS = bsxfun(@(x,y) x.*y, dmaS, diffnorm(:,k));
                
                dmmS = bsxfun(@(x,y) x.*y, rho2_k.*diffnorm, diffnorm(:,k));
                dmmS(:,k) = dmmS(:,k) + rho(:,k).*(diffnorm(:,k).^2 - 1/v(k));
                
                dmgS = bsxfun(@(x,y) x.*y, rho2_k.*(diff2norm - 0.5), diffnorm(:,k));
                dmgS(:,k) = dmgS(:,k) + rho(:,k).*diffnorm(:,k).*(diff2norm(:,k) - 1.5);
                
                %dSm_k and dGm_k
                dSm_k = [dmaS(:,1:self.K-1) dmmS dmgS];
                dsm_k = sum(dSm_k);
                dGm{k} = dSm_k'*S + S'*dSm_k - (dsm_k'*s+s'*dsm_k)./self.N ;
                
                %w.r.t gamma
                dgaS = rho2_k;
                dgaS(:,k) = dgaS(:,k) + rho(:,k);
                dgaS = bsxfun(@(x,y) x.*y, dgaS, diff2norm(:,k) - 0.5);
                
                dgmS = bsxfun(@(x,y) x.*y, rho2_k.*diffnorm, diff2norm(:,k) - 0.5);
                dgmS(:,k) = dgmS(:,k) + rho(:,k).*diffnorm(:,k).*(diff2norm(:,k) - 1.5);
                
                dggS = bsxfun(@(x,y) x.*y, rho2_k.*(diff2norm - 0.5), diff2norm(:,k) - 0.5);
                dggS(:,k) = dggS(:,k) + rho(:,k).*( (diff2norm(:,k) - 0.5).^2 - diff2norm(:,k) );
                
                %dSg_k and dGg_k
                dSg_k = [dgaS(:,1:self.K-1) dgmS dggS];
                dsg_k = sum(dSg_k);
                dGg{k} = dSg_k'*S + S'*dSg_k - (dsg_k'*s+s'*dsg_k)./self.N ;
                
                %g_k prior
                idx = self.K*2-1 + k;
                dGg{k}(idx,idx) = dGg{k}(idx,idx) - self.vp2./v(k) ;
            end
            
            dM = [dGa(1:self.K-1); dGm; dGg];
        end
        
        function InitParameters = get.InitParameters(self)
            %choose randomly some points
            idx = ceil(rand(self.K,1)*self.N);
            mu = self.X(idx);
            
            %split variance equaly to all clusters
            v = ones(self.K,1).*(var(self.X)/self.K);
            
            p = ones(self.K,1)./self.K;
            
            a = log(p) - log(p(self.K));
            a(self.K) =[];
            g = log(v);
            
            InitParameters = [a; mu; g];
        end
        
    end
    
    % Static helper methods
    methods (Static)
        
    end
    
end

