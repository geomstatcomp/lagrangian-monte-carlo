%%% This is to check histogram of samples of Banana Shaped Distribution by
%%% LMC

function []=histBanana()

% Random Numbers...
randn('state', 2012);
rand('twister', 2012);

% simulated data
global N D sigma2y sigma2theta
D=2;
N=100; C=1;
sigma2y=4; sigma2theta=1;
y=C+sqrt(sigma2y).*randn(N,1);


theta1=-10:.1:10; theta2=theta1;
sigma2_2=1/(N/sigma2y-N^2/(N*sigma2y+sigma2y^2/sigma2theta));
mu_2=mean(y)-1/2/sigma2theta*sigma2_2;
pdf2=normpdf(theta2.^2,mu_2,sqrt(sigma2_2));
pdf2=pdf2./trapz(theta2,pdf2);

pdf1=arrayfun(@(theta1)normpdf(theta1,.5,sqrt(sigma2theta))...
    *integral(@(theta2)2*normpdf(theta2.^2,mean(y)-theta1-sigma2y/2/N/sigma2theta,sqrt(sigma2y/N)),0,100),theta1);
pdf1=pdf1./trapz(theta1,pdf1);


fig1=figure(1); clf;
set(fig1,'windowstyle','docked');

%% rhmc
file=dir(['./Results_toy/Banana_rhmc', '*.mat']);
data=open(['./Results_toy/',file.name]);

theta_rmhmc = data.thetaSaved;
subplot(2,3,1);
hist1_rmhmc = hist(theta_rmhmc(:,1),theta1);
b11=bar(theta1,hist1_rmhmc/trapz(theta1,hist1_rmhmc),'hist');
set(b11,'facecolor',[.5,.5,1]);hold on;
xlim([-3,2]);ylim([0,1.4]);
title('RHMC','Fontsize',20);
set(gca,'FontSize',12);
xlabel('\theta_1','Fontsize',15);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
plot(theta1,pdf1,'r-','LineWidth',2); hold off;
subplot(2,3,4);
hist2_rmhmc = hist(theta_rmhmc(:,2),theta2);
b12=bar(theta2,hist2_rmhmc/trapz(theta2,hist2_rmhmc),'hist');
set(b12,'facecolor',[.5,.5,1]);hold on;
xlim([-2.5,2.5]);ylim([0,.7]);
set(gca,'FontSize',12);
xlabel('\theta_2','Fontsize',15);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
plot(theta2,pdf2,'r-','LineWidth',2); hold off;


%% slmc
file=dir(['./Results_toy/Banana_slmc', '*.mat']);
data=open(['./Results_toy/',file.name]);

theta_srmlmc = data.thetaSaved;
subplot(2,3,2);
hist1_slmc = hist(theta_srmlmc(:,1),theta1);
b21=bar(theta1,hist1_slmc/trapz(theta1,hist1_slmc),'hist');
set(b21,'facecolor',[.5,.5,1]);hold on;
xlim([-3,2]);ylim([0,1.4]);
title('sLMC','Fontsize',20);
set(gca,'FontSize',12);
xlabel('\theta_1','Fontsize',15);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
plot(theta1,pdf1,'r-','LineWidth',2); hold off;
subplot(2,3,5);
hist2_slmc = hist(theta_srmlmc(:,2),theta2);
b22=bar(theta2,hist2_slmc/trapz(theta2,hist2_slmc),'hist');
set(b22,'facecolor',[.5,.5,1]);hold on;
xlim([-2.5,2.5]);ylim([0,.7]);
set(gca,'FontSize',12);
xlabel('\theta_2','Fontsize',15);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
plot(theta2,pdf2,'r-','LineWidth',2); hold off;


%% lmc
file=dir(['./Results_toy/Banana_lmc', '*.mat']);
data=open(['./Results_toy/',file.name]);

theta_ermlmc = data.thetaSaved;
subplot(2,3,3);
hist1_elmc = hist(theta_ermlmc(:,1),theta1);
b31=bar(theta1,hist1_elmc/trapz(theta1,hist1_elmc),'hist');
set(b31,'facecolor',[.5,.5,1]);hold on;
xlim([-3,2]);ylim([0,1.4]);
title('LMC','Fontsize',20);
set(gca,'FontSize',12);
xlabel('\theta_1','Fontsize',15);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
plot(theta1,pdf1,'r-','LineWidth',2); hold off;
subplot(2,3,6);
hist2_elmc = hist(theta_ermlmc(:,2),theta2);
b32=bar(theta2,hist2_elmc/trapz(theta2,hist2_elmc),'hist');
set(b32,'facecolor',[.5,.5,1]);hold on;
xlim([-2.5,2.5]);ylim([0,.7]);
set(gca,'FontSize',12);
xlabel('\theta_2','Fontsize',15);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .02 0]);
plot(theta2,pdf2,'r-','LineWidth',2); hold off;



end
