classdef eRMLMCKernel < MCMCKernel & HMCAdaptiveKernel
    %eRMLMCKERNEL Explicit Riemann Manifold Lagrangian Monte Carlo kernel
    %   Vassilios Stathopoulos, stathv@gmail.com
    %   September 2012
    %
    %  Constructors:
    %       ermlmc_kernel = NewERMLMCKernel(target)
    %           Constructs a new eRMLMCKernel with step-size 0.15, 6
    %           leap-frog integration steps and arbitary initial values.
    %           target    : the target density, an object implementing 
    %                           the TargetDistribution interface.
    %
    %       ermlmc_kernel = NewERMLMCKernel(target, PropertyName,Value, ... )
    %           Construct a new eRMLMCKernel with properties spesified as
    %           parameters. Valid property names are:
    %           'StepSize'     : Integration step size.
    %           'LeapFrogSteps': Number of leap-frog integration steps.
    %           'Init'         : Initial values for the chain.
    %       Example: 
    %           ermlmc_kernel = NewERMLMCKernel(myTarget, 'StepSize',0.5);
    %           
    %       eRMLMCKernel(Target,StepSize,LeapFrogSteps,Init)
    %           Target        : The target density.
    %           StepSize      : Integration step size.
    %           LeapFrogStep  : Number of leap-frog integration steps.
    %           Init          : Initial values for the chain.
    %
    %  Properties:
    %       State_x           : Current value of the chain.
    %       State_LogPosterior: Value of the target at State_x.
    %       Target            : Target density.
    %       NumberOfParameters: Length of State_x.
    %       TargetAccRate     : Optimal acceptance rate. Used only for
    %                           tunning.
    %   Methods:
    %       [x acc] = nextSample()
    %           Returns a new random sample from the target density.
    %           x     : The new sample.
    %           acc   : 1 if x is a new sample (accepted proposal) and 0 if
    %                   x is the same as the previous sample in the chain
    %                   (rejected proposal).
    %
    %       printStats()
    %           Prints kernel statistics, step-size, number of leap-frog
    %           integration steps and current value of the target density on
    %           the console.
    %
    
    
    properties
        State_x;
        State_LogPosterior;
        Target;
    end
    
    properties (Dependent)
        NumberOfParameters;
    end
    
    properties (Constant)
        TargetAccRate = 0.82;
    end
    
    properties (Access = private);
        G;
        invG;
        cholG;
        dG;
        TraceInvGdG;
        Gamma1;
        dphi;
    end
    
    methods (Static, Access=private)
        
        function Gamma1 = Christoff(dG,D)
            %Christoffel symbols of the first kind
            if size(dG,1) > size(dG,2) 
                pD = reshape(cell2mat(dG'),D,D,D);
            else
                pD = reshape(cell2mat(dG),D,D,D);
            end
            Gamma1 = 0.5*(permute(pD,[3 2 1]) + permute(pD,[1 3 2]) - pD);
        end
        
        function ld = logDet(A)
            [L, U, P] = lu(A);
            %L is a unit matrix
            du = diag(U);
            c = det(P) * prod(sign(du));
            ld = log(c) + sum(log(abs(du)));    
        end
        
    end
    
    methods
        function self = eRMLMCKernel(Target,StepSize,LeapFrogSteps,Init)
            if ~isa(Target,'TargetDistribution')
                error('eRMLMCKernel:BadConstruction', ...
                    'target should be an instance of TargetDistribution');
            end
            
            if length(Target.InitParameters)~=length(Init)
                error('eRMLMCKernel:BadConstruction', ...
                    'Init must have the same size as Target.InitParamaters');
            end
            
            self.Target = Target;
            self.State_x = Init;
            
            self.StepSize = StepSize;
            self.LeapFrogSteps = LeapFrogSteps;
            
            % initial conditions
            D = self.NumberOfParameters;
            self.State_LogPosterior = Target.LogLikelihood(self.State_x);
            
            self.G = Target.MetricTensor(self.State_x);
            self.cholG = chol(self.G);
            self.invG = inv(self.G);
            
            self.dG = Target.MetricTensorDerivative(self.State_x);
            self.TraceInvGdG = zeros(D,1);
            for d = 1:D 
                self.TraceInvGdG(d) = sum(sum(self.invG.*self.dG{d}'));    
            end
            self.dphi = -Target.LogLikelihoodGradient(self.State_x) + 0.5*self.TraceInvGdG;
            
            self.Gamma1 = self.Christoff(self.dG,D);
            
        end
        
        function NumberOfParameters = get.NumberOfParameters(self)
            NumberOfParameters = length(self.State_x);
        end
        
       
        function [x acc E deltaLogDet Acc] = nextSample(self)
            
            acc = 0;
            x = self.State_x;
            
            
            D = self.NumberOfParameters;
            
            xNew = self.State_x;
            Gnew = self.G;
            cholGnew = self.cholG;
            invGnew = self.invG;
            dGnew = self.dG;
            TraceInvGdGnew =  self.TraceInvGdG;
            Gamma1new = self.Gamma1;
            dphinew = self.dphi;
            
            
            % sample velocities
            v = ( randn(1, D) * chol(invGnew) )';
            vNew = v;
            
            % Randomise number of leap-frog steps.
            RandomSteps = ceil(rand*self.LeapFrogSteps);
            if nargout > 2
                RandomSteps = self.LeapFrogSteps;
            end
            
            VGamma = zeros(D,D);
            % Accumulate determinant to be adjusted in acceptance rate
            Deltalogdet = 0;
            
            if nargout > 2
                E = zeros(RandomSteps,1);
            end
            if nargout > 3
                deltaLogDet = zeros(RandomSteps,1);
            end
            if nargout > 4
                CurrentH = - self.State_LogPosterior - sum(log(diag(self.cholG))) + 0.5*v'*self.G*v;
                Acc = ones(RandomSteps,1)*CurrentH;
            end
            
            for StepNum = 1:RandomSteps
                
                %%%%%%%%%%%%%%%%%%%
                % Update velocity %
                %%%%%%%%%%%%%%%%%%%
                % Make a half step for Velocity
                for d=1:D
                    VGamma(d,:) = vNew'*Gamma1new(:,:,d);
                end
                Deltalogdet = Deltalogdet - eRMLMCKernel.logDet(Gnew + (0.5*self.StepSize)*VGamma);
                
                vNew = (Gnew + (0.5*self.StepSize)*VGamma)\(Gnew*vNew - (0.5*self.StepSize)*dphinew);
                
                for d=1:D
                    VGamma(d,:) = vNew'*Gamma1new(:,:,d);
                end
                Deltalogdet = Deltalogdet + eRMLMCKernel.logDet(Gnew - (0.5*self.StepSize)*VGamma);
                
                
                %%%%%%%%%%%%%%%%%%%%%%%
                % Update q parameters %
                %%%%%%%%%%%%%%%%%%%%%%%
                % Make a full step for the position
                xNew = xNew + (self.StepSize)*vNew;
                
                % Update G based on new parameters
                Gnew = self.Target.MetricTensor(xNew);
                [cholGnew err] = cholcov(Gnew);
                if err ~= 0 
                   warning('eRMLMCKernel:IntegrationError','metric tensor is not positive definite');
                   return;
                end
                invGnew = inv(Gnew);
                dGnew =  self.Target.MetricTensorDerivative(xNew);
                for d = 1:D
                    TraceInvGdGnew(d) = sum(sum(invGnew.*dGnew{d}'));
                end
                dphinew = -self.Target.LogLikelihoodGradient(xNew) + 0.5*TraceInvGdGnew;
                Gamma1new =  self.Christoff(dGnew,D);
                
                
                %%%%%%%%%%%%%%%%%%%
                % Update velocity %
                %%%%%%%%%%%%%%%%%%%
                % Make a half step for Velocity
                for d=1:D
                    VGamma(d,:) = vNew'*Gamma1new(:,:,d);
                end
                Deltalogdet = Deltalogdet - eRMLMCKernel.logDet(Gnew + (0.5*self.StepSize)*VGamma);
                
                vNew = (Gnew + (0.5*self.StepSize)*VGamma)\(Gnew*vNew - (0.5*self.StepSize)*dphinew);
                
                for d=1:D
                    VGamma(d,:) = vNew'*Gamma1new(:,:,d);
                end
                Deltalogdet = Deltalogdet + eRMLMCKernel.logDet(Gnew - (0.5*self.StepSize)*VGamma);
                
                if nargout > 2
                    %save current H;
                    LLnew = self.Target.LogLikelihood(xNew);
                    ProposedH = - LLnew -sum(log(diag(cholGnew))) + 0.5*vNew'*Gnew*vNew;
                    E(StepNum) = ProposedH;
                    if nargout > 4
                        Acc(StepNum) =  -ProposedH + Acc(StepNum) ;
                    end
                end
                if nargout > 3
                    %save current deltaLogDet;
                    deltaLogDet(StepNum) = Deltalogdet;
                end
                
            end
                        
            % Calculate proposed H value
            LLnew = self.Target.LogLikelihood(xNew);
            ProposedH = - LLnew -sum(log(diag(cholGnew))) + 0.5*vNew'*Gnew*vNew;
            CurrentH = - self.State_LogPosterior - sum(log(diag(self.cholG))) + 0.5*v'*self.G*v;
            
            % Accept according to ratio
            Ratio = -ProposedH + CurrentH + Deltalogdet;
            
            if (isfinite(Ratio) && (Ratio > min([0,log(rand)])))
            %if Ratio > 0 || (Ratio > log(rand))
                self.State_LogPosterior = LLnew;
                self.State_x = xNew;
                self.G = Gnew;
                self.cholG = cholGnew;
                self.invG  = invGnew;
                self.dG = dGnew;
                self.TraceInvGdG = TraceInvGdGnew;
                self.Gamma1 = Gamma1new;
                self.dphi = dphinew;
                
                x = xNew;
                acc = 1;
            end
            
        end
        
        function printStats(self)
            fprintf(1, 'eRMLMCKernel Kernel: StepSize = %g, LeapFrogSteps = %u\n',...
                self.StepSize, self.LeapFrogSteps);
            fprintf(1, 'Current LogPosterior = %g\n',...
                self.State_LogPosterior);
        end
        
    end
    
    methods(Static)
        
        %Convinience factory method
        function ermlmc_kernel = NewERMLMCKernel(target,varargin)
            
            if ~isa(target,'TargetDistribution')
                error('eRMLMCKernel:BadConstruction', ...
                    'target should be an instance of TargetDistribution');
            end
            
            argP = inputParser;
            argP.KeepUnmatched = true;
            argP.addParamValue('LeapFrogSteps', 6,  @(x) x > 0 );
            argP.addParamValue('StepSize', 0.15,  @(x) x > 0 );
            argP.addParamValue('Init', 'model');
            argP.parse( varargin{:} );
            
            
            NumOfLeapFrogSteps = argP.Results.LeapFrogSteps;
            StepSize = argP.Results.StepSize;
            Init = argP.Results.Init;
            
            
            if ~(isnumeric(Init) && isvector(Init))
                Init = target.InitParameters;
            end
            
            ermlmc_kernel = eRMLMCKernel(target,StepSize,NumOfLeapFrogSteps,Init);
        end
    end
    
end


