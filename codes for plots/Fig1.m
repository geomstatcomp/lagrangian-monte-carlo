%%% This is to illustrate the difference of RWM, HMC, RHMC in terms of
%%% exploring parameter space of a banana shape.

function [] = comp(NumOfIterations, BurnIn)

if(nargin==0)
    NumOfIterations    = 10;
    BurnIn             = 0;
elseif(nargin==1)
    BurnIn = floor(.2*NumOfIterations);
elseif(nargin>2)
    error('wrong number of inputs!');
end

% Random Numbers...
randn('state', 2012);
rand('twister', 2012);

% simulated data
global N sigma2y sigma2theta
N=100; D=2; C=1;
sigma2y=4; sigma2theta=1;
y=C+sqrt(sigma2y).*randn(N,1);

% settings
Trajectory = 1;
NumOfNewtonSteps = 5;

% draw posterior
[theta1, theta2] = meshgrid([-2:.1:2],[-2:.1:2]);
thetaseq = [theta1(:) theta2(:)];
potent = reshape(cellfun(@(theta)U(y,theta'), num2cell(thetaseq,2)),size(theta1));
% normalize the density
nadir = min(min(potent));
postdf = exp(-potent+nadir)+10;
postdf = postdf./sum(sum(postdf));

% fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
% surf(theta1,theta2,postdf);
% title('Posterior Density Surface');
% drawnow;



fig1=figure(1); clf;
set(fig1,'windowstyle','docked');
set(0,'CurrentFigure',fig1);
hold on;

% RWM
subplot(1,3,1);
contour(theta1,theta2,postdf,5);
title('Sampling Path of RWM','Fontsize',20);
set(gca,'FontSize',15);
xlabel('\theta_1','Fontsize',20);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .01 0]);
ylabel('\theta_2','Fontsize',20);
hold on;

% sampling setting
NumOfLeapFrogSteps = 17; % 13/20
StepSize = Trajectory/NumOfLeapFrogSteps;

H_RWM = zeros((NumOfIterations-BurnIn)*NumOfLeapFrogSteps,1);
% Initialize
theta=[-1;1.4];
plot(theta(1), theta(2),'bo','MarkerFaceColor','k'); hold on;
% Calculate joint log likelihood for current theta
CurrentU  = U(y,theta);

Currenttheta = theta;
accepted = 0;
for IterationNum = 1:NumOfIterations*NumOfLeapFrogSteps
    
    %IterationNum
    theta = Currenttheta;
    
    % Make proposal by random walk
    thetaNew = theta + StepSize.*randn(D,1);
    
    PropPath = plot([theta(1),thetaNew(1)], [theta(2),thetaNew(2)],'r-','LineWidth',2); pause(.1);
    
    % Calculate proposed H value
    ProposedU = U(y,thetaNew);
    
    % Record Energys
    H_RWM(IterationNum) = ProposedU;
    
    
    % Accept according to ratio
    Ratio = -ProposedU + CurrentU;
    
    if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
        Currenttheta= thetaNew;
        CurrentU = ProposedU;
        if mod(IterationNum,NumOfLeapFrogSteps)==0
            plot(thetaNew(1), thetaNew(2),'bo','MarkerFaceColor','k'); pause(1/NumOfLeapFrogSteps);
        end
        accepted = accepted +1;
    else
        delete(PropPath);
    end
        
end
drawnow;
display(['Acceptance rate for RWM: ', num2str(accepted/(NumOfIterations*NumOfLeapFrogSteps))]);


% HMC
subplot(1,3,2);
contour(theta1,theta2,postdf,5);
title('Sampling Path of HMC','Fontsize',20);
set(gca,'FontSize',15);
xlabel('\theta_1','Fontsize',20);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .01 0]);
ylabel('\theta_2','Fontsize',20);
hold on;

% sampling setting
NumOfLeapFrogSteps = 7; % 7/20
StepSize = Trajectory/NumOfLeapFrogSteps;

H_HMC = zeros((NumOfIterations-BurnIn)*NumOfLeapFrogSteps,1);
% Initialize
theta=[-1;1.4];
plot(theta(1), theta(2),'bo','MarkerFaceColor','k'); hold on;
% Calculate joint log likelihood for current theta
CurrentU  = U(y,theta);

Currenttheta = theta;
accepted = 0;
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    theta = Currenttheta;
    thetaNew = theta;
    
    % pre-leapfrog calculation
    % terms other than quadratic one
    dphi = U(y,thetaNew,1);
    
    % propose velocity
    Velocity = randn(D,1);
    
    % Calculate current H value
    CurrentH  = CurrentU + sum(Velocity.^2)/2;
    
    
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        Velocity = Velocity- (StepSize/2)*dphi;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        % Make a full step for the position
        thetaNew = thetaNew + StepSize*Velocity;
        PropPath(StepNum) = plot([theta(1),thetaNew(1)], [theta(2),thetaNew(2)],'r-','LineWidth',2); pause(.1);
        
        
        % terms other than quadratic one
        dphi = U(y,thetaNew,1);
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        Velocity = Velocity- (StepSize/2)*dphi;
        
        
        theta = thetaNew;
        % Record Energys
        H_HMC((IterationNum-1)*NumOfLeapFrogSteps+StepNum) = U(y,thetaNew)+ sum(Velocity.^2)/2;
        
    end
    
    % Calculate proposed H value
    ProposedU = U(y,thetaNew);
    ProposedH = H_HMC(IterationNum*NumOfLeapFrogSteps);
    
    
    % Accept according to ratio
    Ratio = -ProposedH + CurrentH;
    
    if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
        Currenttheta= thetaNew;
        CurrentU = ProposedU;
        plot(thetaNew(1), thetaNew(2),'bo','MarkerFaceColor','k'); pause(1);
        accepted = accepted +1;
    else
        delete(PropPath);
    end
        
end
drawnow;
display(['Acceptance rate for HMC: ', num2str(accepted/NumOfIterations)]);


% RHMC
subplot(1,3,3);
contour(theta1,theta2,postdf,5);
title('Sampling Path of RHMC','Fontsize',20);
set(gca,'FontSize',15);
xlabel('\theta_1','Fontsize',20);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .01 0]);
ylabel('\theta_2','Fontsize',20);
hold on;

% sampling settings
NumOfLeapFrogSteps = 5; %4/20
StepSize = Trajectory/NumOfLeapFrogSteps;

H_RHMC = zeros((NumOfIterations-BurnIn)*NumOfLeapFrogSteps,1);
% Initialize
theta=[-1;1.4];
plot(theta(1), theta(2),'bo','MarkerFaceColor','k'); hold on;
% Calculate joint log likelihood for current theta
CurrentU  = U(y,theta);

Currenttheta = theta;
accepted = 0;
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    theta = Currenttheta;
    thetaNew = theta;
    
    % pre-leapfrog calculation
    % Calculate G
    [G InvG dG] = Met(thetaNew,[0 -1 1]);
    CholG = chol(G);
    % Calculate the partial derivatives dG/dw
    for d = 1:D
        TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
    end
    % terms other than quadratic one
    dphi = U(y,thetaNew,1) + 0.5*TraceInvGdG';
    
    % propose momentum
    Momentum = (randn(1,D)*CholG)';
    
    % Calculate current H value
    CurrentLogDet = sum(log(diag(CholG)));
    CurrentH  = CurrentU + CurrentLogDet + (Momentum'*InvG*Momentum)/2;
    
    
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        % Multiple fixed point iteration
        PM = Momentum;
        for FixedIter = 1:NumOfNewtonSteps
            MomentumHist(FixedIter,:) = PM;
            
            InvGMomentum = InvG*PM;
            for d = 1:D
                dQuadTerm(d)  = 0.5*(InvGMomentum'*dG(:,:,d)*InvGMomentum);
            end
            
            PM = Momentum + (StepSize/2)*(-dphi + dQuadTerm');
        end
        Momentum = PM;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        %%% Multiple Fixed Point Iteration %%%
        FixedInvGMomentum  = G\Momentum;
        
        Ptheta = thetaNew;
        for FixedIter = 1:NumOfNewtonSteps
            thetaHist(FixedIter,:) = Ptheta;
            
            InvGMomentum = Met(Ptheta)\Momentum;
            Ptheta = thetaNew + (StepSize/2)*(FixedInvGMomentum + InvGMomentum);
        end
        thetaNew = Ptheta;
        PropPath(StepNum) = plot([theta(1),thetaNew(1)], [theta(2),thetaNew(2)],'r-','LineWidth',2); pause(.1);
        
        
        % Update G based on new parameters
        [G InvG dG] = Met(thetaNew,[0 -1 1]);
        CholG = chol(G);
        % Update the partial derivatives dG/dw
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
        end
        % terms other than quadratic one
        dphi = U(y,thetaNew,1) + 0.5*TraceInvGdG';
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        InvGMomentum = InvG*Momentum;
        for d = 1:D
            dQuadTerm(d) = 0.5*(InvGMomentum'*dG(:,:,d)*InvGMomentum);
        end
        Momentum = Momentum + (StepSize/2)*(-dphi + dQuadTerm');
        
        
        theta = thetaNew;
        % Record Energys
        H_RHMC((IterationNum-1)*NumOfLeapFrogSteps+StepNum) = U(y,thetaNew) + sum(log(diag(CholG))) + (Momentum'*InvG*Momentum)/2;
        
    end
    
    % Calculate proposed H value
     ProposedU = U(y,thetaNew);
%     ProposedLogDet = sum(log(diag(chol(G))));
%     ProposedH = ProposedU + ProposedLogDet + (Momentum'*InvG*Momentum)/2;
    ProposedH = H_RHMC(IterationNum*NumOfLeapFrogSteps);
    
    
    % Accept according to ratio
    Ratio = -ProposedH + CurrentH;
    
    if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
        Currenttheta= thetaNew;
        CurrentU = ProposedU;
        plot(thetaNew(1), thetaNew(2),'bo','MarkerFaceColor','k'); pause(1);
        accepted = accepted +1;
    else
        for StepNum = 1:NumOfLeapFrogSteps
            delete(PropPath(StepNum));
        end
    end
    
end
drawnow;
display(['Acceptance rate for RHMC: ', num2str(accepted/NumOfIterations)]);

% imwrite(fig1,'illust.png');
% print(fig1,'illust.png');

end



% functions needed
function [output] = U(y,theta,der)
if(nargin<3)
    der=0;
end

global N sigma2y sigma2theta

if der==0
    logpri = -ecmnobj(theta,0,sigma2theta);
    loglik = -ecmnobj(y,theta(1)+theta(2)^2,sigma2y); %training likelihood
    output = -(loglik+logpri);
elseif der==1
    dlogpri = -theta./sigma2theta;
    dloglik = (sum(y)-N*(theta(1)+theta(2)^2))/sigma2y*[1;2*theta(2)];
    output  = -(dloglik + dlogpri);
else
    disp('wrong choice of der!')
end

end


function [G InvG dG Gamma1] = Met(theta,opt)
if(nargin<2)
    opt=0;
end

global N sigma2y sigma2theta

G = [N/sigma2y+1/sigma2theta, 2*N*theta(2)/sigma2y; 2*N*theta(2)/sigma2y, 4*N*theta(2)^2/sigma2y+1/sigma2theta];
if all(opt==0)
    InvG=NaN; dG=NaN; Gamma1=NaN;
else
    if any(opt==-1)
        InvG = inv(G);
    end
    if any(opt==1)
        dG = zeros(repmat(2,1,3));
        dG(:,:,2) = [0, 2*N/sigma2y; 2*N/sigma2y, 8*N*theta(2)/sigma2y];
        if any(opt==3)
            Gamma1 = .5*(permute(dG,[1,3,2]) + permute(dG,[3,2,1]) - dG);
        else
            Gamma1=NaN;
        end
    else
        dG=NaN; Gamma1=NaN;
    end
end

end

