function [ settings ] = factsett( levels, run )
%FACTSETT Mixed-level full-factorial designs.
%   SETTINGS=FACTSETT(LEVELS,run) creates a vector SETTINGS containing the
%   factor settings for a the run specified by RUN. The vector LEVELS
%   specifies the number of unique settings in each column of the design.
%
%   Example:
%       LEVELS = [2 4 3];
%       RUN = 14;
%       SETTINGS = FACTSETT(LEVELS, RUN);
%       SETTINGS = 
%            2   3   2
%   This generates a 24 run design with 2 levels in the first column,
%   4 in the second column, and 3 in the third column. SETTINGS is the 14
%   row of the design matrix.
%
%   Vassilios Stathopoulos stathv@gmail.com September 2011.
%

    [m,n] = size(levels);
    % Error checking
    if ~isfloat(levels)
        levels = double(levels);
    end
    
    if min(m,n) ~= 1
        error('factsett:VectorRequired','levels must be a vector');
    end
    
    if any(floor(levels) ~= levels) || any(levels < 1)
        error('factsett:IntegersRequired', 'levels must be integers');
    end
    
    ssize = prod(levels);
    if run < 1 || run > ssize
        error('factsett:InvalidParam',['run should be in [1:' num2str(ssize) ']' ]);
    end
    
    den = [1 cumprod(levels(1:end-1))];
    settings = mod( ceil(run./den)-1, levels) +1 ;
        
end

