%%% This is to illustrate the difference of RHMC,sLMC,LMC in terms of
%%% exploring parameter space of a banana shape.

function [] = comp(NumOfIterations, BurnIn)

if(nargin==0)
    NumOfIterations    = 10;
    BurnIn             = 0;
elseif(nargin==1)
    BurnIn = floor(.2*NumOfIterations);
elseif(nargin>2)
    error('wrong number of inputs!');
end

% Random Numbers...
randn('state', 2012);
rand('twister', 2012);

% simulated data
global N sigma2y sigma2theta
N=100; D=2; C=1;
sigma2y=4; sigma2theta=1;
y=C+sqrt(sigma2y).*randn(N,1);

% settings
Trajectory = 1.45;
NumOfNewtonSteps = 5;

% draw posterior
[theta1, theta2] = meshgrid([-2:.1:2],[-2:.1:2]);
thetaseq = [theta1(:) theta2(:)];
potent = reshape(cellfun(@(theta)U(y,theta'), num2cell(thetaseq,2)),size(theta1));
% normalize the density
nadir = min(min(potent));
postdf = exp(-potent+nadir)+10;
postdf = postdf./sum(sum(postdf));

% fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
% surf(theta1,theta2,postdf);
% title('Posterior Density Surface');
% drawnow;



fig1=figure(1); clf;
set(fig1,'windowstyle','docked');
set(0,'CurrentFigure',fig1);
hold on;


% RHMC
subplot(1,3,1);
contour(theta1,theta2,postdf,5);
title('Sampling Path of RHMC','Fontsize',20);
set(gca,'FontSize',15);
xlabel('\theta_1','Fontsize',20);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .01 0]);
ylabel('\theta_2','Fontsize',20);
hold on;

% sampling setting
NumOfLeapFrogSteps = 10;
StepSize = Trajectory/NumOfLeapFrogSteps;

% Initialize
theta=[-1;1.4];
plot(theta(1), theta(2),'bo','MarkerFaceColor','k'); hold on;
% Calculate joint log likelihood for current theta
CurrentU  = U(y,theta);

Currenttheta = theta;
accepted = 0;
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    theta = Currenttheta;
    thetaNew = theta;
    
    % pre-leapfrog calculation
    % Calculate G
    [G InvG dG] = Met(thetaNew,[0 -1 1]);
    CholG = chol(G);
    % Calculate the partial derivatives dG/dw
    for d = 1:D
        TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
    end
    % terms other than quadratic one
    dphi = U(y,thetaNew,1) + 0.5*TraceInvGdG';
    
    % propose momentum
    Momentum = (randn(1,D)*CholG)';
    
    % Calculate current H value
    CurrentLogDet = sum(log(diag(CholG)));
    CurrentH  = CurrentU + CurrentLogDet + (Momentum'*InvG*Momentum)/2;
    
    
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        % Multiple fixed point iteration
        PM = Momentum;
        for FixedIter = 1:NumOfNewtonSteps
            MomentumHist(FixedIter,:) = PM;
            
            InvGMomentum = InvG*PM;
            for d = 1:D
                dQuadTerm(d)  = 0.5*(InvGMomentum'*dG(:,:,d)*InvGMomentum);
            end
            
            PM = Momentum + (StepSize/2)*(-dphi + dQuadTerm');
        end
        Momentum = PM;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        %%% Multiple Fixed Point Iteration %%%
        FixedInvGMomentum  = G\Momentum;
        
        Ptheta = thetaNew;
        for FixedIter = 1:NumOfNewtonSteps
            thetaHist(FixedIter,:) = Ptheta;
            
            InvGMomentum = Met(Ptheta)\Momentum;
            Ptheta = thetaNew + (StepSize/2)*(FixedInvGMomentum + InvGMomentum);
        end
        thetaNew = Ptheta;
        PropPath(StepNum) = plot([theta(1),thetaNew(1)], [theta(2),thetaNew(2)],'r-','LineWidth',2); pause(.1);
        
        
        % Update G based on new parameters
        [G InvG dG] = Met(thetaNew,[0 -1 1]);
        % Update the partial derivatives dG/dw
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
        end
        % terms other than quadratic one
        dphi = U(y,thetaNew,1) + 0.5*TraceInvGdG';
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        InvGMomentum = InvG*Momentum;
        for d = 1:D
            dQuadTerm(d) = 0.5*(InvGMomentum'*dG(:,:,d)*InvGMomentum);
        end
        Momentum = Momentum + (StepSize/2)*(-dphi + dQuadTerm');
        
        
        theta = thetaNew;
        
    end
    
    % Calculate proposed H value
    ProposedU = U(y,thetaNew);
    ProposedLogDet = sum(log(diag(chol(G))));
    ProposedH = ProposedU + ProposedLogDet + (Momentum'*InvG*Momentum)/2;
    
    
    % Accept according to ratio
    Ratio = -ProposedH + CurrentH;
    
    if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
        Currenttheta= thetaNew;
        CurrentU = ProposedU;
        plot(thetaNew(1), thetaNew(2),'bo','MarkerFaceColor','k'); pause(.5);
        accepted = accepted +1;
    else
        for StepNum = 1:NumOfLeapFrogSteps
            delete(PropPath(StepNum));
        end
        plot(thetaNew(1), thetaNew(2),'bx','Markersize',10,'Linewidth',2); pause(.5);
    end
    
end
drawnow;
display(['Acceptance rate for RHMC: ', num2str(accepted/NumOfIterations)]);



% sLMC
subplot(1,3,2);
contour(theta1,theta2,postdf,5);
title('Sampling Path of sLMC','Fontsize',20);
set(gca,'FontSize',15);
xlabel('\theta_1','Fontsize',20);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .01 0]);
ylabel('\theta_2','Fontsize',20);
hold on;

% sampling setting
NumOfLeapFrogSteps = 10;
StepSize = Trajectory/NumOfLeapFrogSteps;


% Initialize
theta=[-1;1.4];
plot(theta(1), theta(2),'bo','MarkerFaceColor','k'); hold on;
% Calculate joint log likelihood for current theta
CurrentU  = U(y,theta);

Currenttheta = theta;
accepted = 0;
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    theta = Currenttheta;
    thetaNew = theta;
    
    % pre-leapfrog calculation
    % Calculate G
    [G InvG dG Gamma1] = Met(thetaNew,[0 -1 1 3]);
    CholInvG = chol(InvG);
    % Calculate the partial derivatives dG/dq
    for d = 1:D
        TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
    end
    % terms other than quadratic one
    dphi = U(y,thetaNew,1) + 0.5*TraceInvGdG';
    
    % propose velocity
    Velocity = (randn(1,D)*CholInvG)';
    
    % Calculate current H value
    CurrentLogDet = - sum(log(diag(CholInvG)));
    CurrentH  = CurrentU + CurrentLogDet + (Velocity'*G*Velocity)/2;
    
    % Accumulate determinant to be adjusted in acceptance rate
    Deltalogdet = 0;
    
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Multiple fixed point iteration
        PV = Velocity; dQuadTerm = zeros(D,1);
        for FixedIter = 1:NumOfNewtonSteps
            VelocityHist(FixedIter,:) = PV;
            
            for d = 1:D
                dQuadTerm(d) = PV'*Gamma1(:,:,d)*PV;
            end
            PV = Velocity - (StepSize/2)*(InvG*(dphi + dQuadTerm));
        end
        Velocity = PV;
        
        % Record Delta log-determinant
        VGamma1 = zeros(D);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(G+StepSize*VGamma1));

        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        thetaNew = thetaNew + StepSize*Velocity;
        PropPath(StepNum) = plot([theta(1),thetaNew(1)], [theta(2),thetaNew(2)],'r-','LineWidth',2); pause(.1);
        
        % Update G based on new parameters
        [G InvG dG Gamma1] = Met(thetaNew,[0 -1 1 3]);
        % Update the partial derivatives dG/dq
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
        end
        % terms other than quadratic one
        dphi = U(y,thetaNew,1) + 0.5*TraceInvGdG';
        
        % Record Delta log-determinant
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(G-StepSize*VGamma1));

        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
%         for d = 1:D
%             dQuadTerm(d) = Velocity'*Gamma1(:,:,d)*Velocity;
%         end
        dQuadTerm = VGamma1*Velocity;
        Velocity = Velocity - (StepSize/2)*(InvG*(dphi + dQuadTerm));
        
        theta = thetaNew;
        
    end
    
    % Calculate proposed H value
    ProposedU = U(y,thetaNew);
    ProposedLogDet = sum(log(diag(chol(G))));
    ProposedH = ProposedU + ProposedLogDet + (Velocity'*G*Velocity)/2;
    
    
    % Accept according to ratio
    Ratio = -ProposedH + CurrentH + Deltalogdet;
    
    if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
        Currenttheta= thetaNew;
        CurrentU = ProposedU;
        plot(thetaNew(1), thetaNew(2),'bo','MarkerFaceColor','k'); pause(1);
        accepted = accepted +1;
    else
        plot(thetaNew(1), thetaNew(2),'rs','MarkerFaceColor','k'); pause(1);
        delete(PropPath);
    end
    
end
drawnow;
display(['Acceptance rate for sLMC: ', num2str(accepted/NumOfIterations)]);


% LMC
subplot(1,3,3);
contour(theta1,theta2,postdf,5);
title('Sampling Path of LMC','Fontsize',20);
set(gca,'FontSize',15);
xlabel('\theta_1','Fontsize',20);
xlabh = get(gca,'XLabel');
set(xlabh,'Position',get(xlabh,'Position') - [0 .01 0]);
ylabel('\theta_2','Fontsize',20);
hold on;

% sampling settings
NumOfLeapFrogSteps = 10;
StepSize = Trajectory/NumOfLeapFrogSteps;

% Initialize
theta=[-1;1.4];
plot(theta(1), theta(2),'bo','MarkerFaceColor','k'); hold on;
% Calculate joint log likelihood for current theta
CurrentU  = U(y,theta);

Currenttheta = theta;
accepted = 0;
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    theta = Currenttheta;
    thetaNew = theta;
    
    % pre-leapfrog calculation
    % Calculate G
    [G InvG dG Gamma1] = Met(thetaNew,[0 -1 1 3]);
    CholInvG = chol(InvG);
    % Calculate the partial derivatives dG/dq
    for d = 1:D
        TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
    end
    % terms other than quadratic one
    dphi = U(y,thetaNew,1)+ 0.5*TraceInvGdG';
    
    % propose velocity
    Velocity = (randn(1,D)*CholInvG)';
    
    % Calculate current H value
    CurrentLogDet = sum(log(diag(CholInvG))); % adjusted
    CurrentH = CurrentU + CurrentLogDet + (Velocity'*G*Velocity)/2;
    
    % Accumulate determinant to be adjusted in acceptance rate
    Deltalogdet = 0;
    
    
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        VGamma1 = zeros(D);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(G+(StepSize/2)*VGamma1));
        Velocity = (G+(StepSize/2)*VGamma1)\(G*Velocity- (StepSize/2)*dphi);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(G-(StepSize/2)*VGamma1));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        % Make a full step for the position
        thetaNew = thetaNew + StepSize*Velocity;
        PropPath(StepNum) = plot([theta(1),thetaNew(1)], [theta(2),thetaNew(2)],'r-','LineWidth',2); pause(.1);
        
        
        % Update G based on new parameters
        [G InvG dG Gamma1] = Met(thetaNew,[0 -1 1 3]);
        % Update the partial derivatives dG/dq
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
        end
        % terms other than quadratic one
        dphi = U(y,thetaNew,1)+ 0.5*TraceInvGdG';
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(G+(StepSize/2)*VGamma1));
        Velocity = (G+(StepSize/2)*VGamma1)\(G*Velocity- (StepSize/2)*dphi);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(G-(StepSize/2)*VGamma1));
        
        
        theta = thetaNew;
        
    end
    
    % Calculate proposed H value
    ProposedU = U(y,thetaNew);
    ProposedLogDet = -sum(log(diag(chol(G))));
    ProposedH = ProposedU + ProposedLogDet + (Velocity'*G*Velocity)/2;
    
    
    % Accept according to ratio
    Ratio = -ProposedH + CurrentH + Deltalogdet;
    
    if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
        Currenttheta= thetaNew;
        CurrentU = ProposedU;
        plot(thetaNew(1), thetaNew(2),'bo','MarkerFaceColor','k'); pause(.5);
        accepted = accepted +1;
    else
        for StepNum = 1:NumOfLeapFrogSteps
            delete(PropPath(StepNum));
        end
        plot(thetaNew(1), thetaNew(2),'bx','Markersize',10,'Linewidth',2); pause(.5);
    end
    
end
drawnow;
display(['Acceptance rate for LMC: ', num2str(accepted/NumOfIterations)]);



end



% functions needed
function [output] = U(y,theta,der)
if(nargin<3)
    der=0;
end

global N sigma2y sigma2theta

if der==0
    logpri = -ecmnobj(theta,0,sigma2theta);
    loglik = -ecmnobj(y,theta(1)+theta(2)^2,sigma2y); %training likelihood
    output = -(loglik+logpri);
elseif der==1
    dlogpri = -theta./sigma2theta;
    dloglik = (sum(y)-N*(theta(1)+theta(2)^2))/sigma2y*[1;2*theta(2)];
    output  = -(dloglik + dlogpri);
else
    disp('wrong choice of der!')
end

end


function [G InvG dG Gamma1] = Met(theta,opt)
if(nargin<2)
    opt=0;
end

global N sigma2y sigma2theta

G = [N/sigma2y+1/sigma2theta, 2*N*theta(2)/sigma2y; 2*N*theta(2)/sigma2y, 4*N*theta(2)^2/sigma2y+1/sigma2theta];
if all(opt==0)
    InvG=NaN; dG=NaN; Gamma1=NaN;
else
    if any(opt==-1)
        InvG = inv(G);
    end
    if any(opt==1)
        dG = zeros(repmat(2,1,3));
        dG(:,:,2) = [0, 2*N/sigma2y; 2*N/sigma2y, 8*N*theta(2)/sigma2y];
        if any(opt==3)
            Gamma1 = .5*(permute(dG,[1,3,2]) + permute(dG,[3,2,1]) - dG);
        else
            Gamma1=NaN;
        end
    else
        dG=NaN; Gamma1=NaN;
    end
end

end

