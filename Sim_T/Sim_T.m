%%% This is to compare the sampling efficiency of HMC,RHMC,sLMC,LMC in
%%% sampling multivariate T distribution

function [] = comp(NumOfIterations, BurnIn)

if(nargin==0)
    NumOfIterations    = 6000;
    BurnIn             = 1000;
elseif(nargin==1)
    BurnIn = floor(.2*NumOfIterations);
elseif(nargin>2)
    error('wrong number of inputs!');
end

% set rng seed
s = RandStream('mt19937ar','Seed',2013);
RandStream.setGlobalStream(s);

% multivariate T
D=50; nu=1000;
K=1e4; % kappa, condition number, the ratio between the largest and smallest eigen-values.

% settings
Trajectory = 4;
NumOfNewtonSteps = 5;

Alg = [1 1 1 1];
NLeap = [600 6 6 6];

Sigma=gallery('randsvd',D,-K,1);
global InvSigma
InvSigma = inv(Sigma);


% % draw posterior
% [x1, x2] = meshgrid([-4:.1:4],[-4:.1:4]);
% xseq = [x1(:) x2(:)];
% potent = reshape(cellfun(@(x)U(x',nu,Sigma([1,D],[1,D])), num2cell(xseq,2)),size(x1));
% % normalize the density
% nadir = min(min(potent));
% postdf = exp(-potent+nadir)+10;
% postdf = postdf./sum(sum(postdf));
% 
% fig1=figure(1); clf;
% set(fig1,'windowstyle','docked');
% contour(x1,x2,postdf);
% title('T-Distribution');
% drawnow;



%% HMC

if(Alg(1))

% sampling setting
NumOfLeapFrogSteps = NLeap(1);
StepSize = Trajectory/NumOfLeapFrogSteps;

xSaved = zeros(NumOfIterations-BurnIn,D);
% Initialize
x = zeros(D,1);
% Calculate energey function
CurrentU  = U(x,nu,Sigma);
dU = U(x,nu,Sigma,1);

accepted = 0;
disp(' ');
disp('Running HMC...');
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    xNew = x;
    dUNew= dU;
    
    % propose velocity
    Velocity = randn(D,1);
    
    % Calculate current H value
    CurrentH  = CurrentU + sum(Velocity.^2)/2;
    
    randL = ceil(rand*NumOfLeapFrogSteps);
    
    % Perform leapfrog steps
    for StepNum = 1:randL
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        Velocity = Velocity- (StepSize/2)*dUNew;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        % Make a full step for the position
        xNew = xNew + StepSize*Velocity;
        
        % update dU
        dUNew = U(xNew,nu,Sigma,1);
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        Velocity = Velocity- (StepSize/2)*dUNew;
        
    end
    
    % Calculate proposed H value
    ProposedU = U(xNew,nu,Sigma);
    ProposedH = ProposedU + sum(Velocity.^2)/2;
    
    
    % Accept according to ratio
    Ratio = -ProposedH + CurrentH;
    
    if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
        x = xNew; dU = dUNew;
        CurrentU = ProposedU;
        acceptedyes = 1;
    else
        acceptedyes = 0;
    end
    
    % Save samples if required
    if IterationNum > BurnIn
        xSaved(IterationNum-BurnIn,:) = x';
        accepted = accepted + acceptedyes;
        if mod(IterationNum,100) == 0
            disp([num2str(IterationNum) ' iterations completed.'])
            disp(accepted/(IterationNum-BurnIn));
        end
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing samples.')
        tic;
    end
        
end

TimeTaken=toc;
CurTime = fix(clock);
acpt = accepted/(NumOfIterations-BurnIn);
save(['Results/SimT_hmc_D',num2str(D),'_K',num2str(K,'%.0e'),'_' num2str(CurTime),'.mat'],'xSaved','StepSize','NumOfLeapFrogSteps','acpt','TimeTaken')
disp(' ');
disp(['Accpetance Rate of HMC: ',num2str(acpt)]);
disp(' ');
currentfolder=cd('./Results/');
CalculateStatistics('SimT_hmc',strcat('D',num2str(D),'_K',num2str(K,'%.0e')));
cd(currentfolder);

end

warning('off');
%% RHMC

if(Alg(2))

% sampling setting
NumOfLeapFrogSteps = NLeap(2);
StepSize = Trajectory/NumOfLeapFrogSteps;

xSaved = zeros(NumOfIterations-BurnIn,D);
% Initialize
x = zeros(D,1);
% Calculate potential energy
CurrentU  = U(x,nu,Sigma);
% Calculate G
[G InvG dG] = Met(x,nu,Sigma,[0 -1 1]);
CholG = chol(G);
% Calculate the partial derivatives dG/dw
for d = 1:D
    TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
end
% terms other than quadratic one
dphi = U(x,nu,Sigma,1) + 0.5*TraceInvGdG';

accepted = 0;
disp(' ');
disp('Running RHMC...');
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    xNew = x;
    CholGNew=CholG; InvGNew=InvG; dGNew=dG;
    dphiNew=dphi;
    
    % propose momentum
    Momentum = (randn(1,D)*CholGNew)';
    
    % Calculate current H value
    CurrentLogDet = sum(log(diag(CholGNew)));
    CurrentH  = CurrentU + CurrentLogDet + (Momentum'*InvGNew*Momentum)/2;
    
    randL = ceil(rand*NumOfLeapFrogSteps);
    
    % Perform leapfrog steps
    for StepNum = 1:randL
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        % Multiple fixed point iteration
        PM = Momentum;
        for FixedIter = 1:NumOfNewtonSteps
            MomentumHist(FixedIter,:) = PM;
            
            InvGMomentum = InvGNew*PM;
            for d = 1:D
                dQuadTerm(d)  = 0.5*(InvGMomentum'*dGNew(:,:,d)*InvGMomentum);
            end
            
            PM = Momentum + (StepSize/2)*(-dphiNew + dQuadTerm');
        end
        Momentum = PM;
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        %%% Multiple Fixed Point Iteration %%%
        Px = xNew;
        for FixedIter = 1:NumOfNewtonSteps
            xHist(FixedIter,:) = Px;
            
            InvGMomentum = InvGNew*Momentum;
            if FixedIter==1
                FixedInvGMomentum = InvGMomentum;
            end
            Px = xNew + (StepSize/2)*(FixedInvGMomentum + InvGMomentum);
            if FixedIter~=NumOfNewtonSteps
                [~,InvGNew] = Met(Px,nu,Sigma,[0 -1]);
            else
                [G InvGNew dGNew] = Met(Px,nu,Sigma,[0 -1 1]);
            end
            
        end
        xNew = Px;
        
        
        % Update G based on new parameters
        % Update the partial derivatives dG/dw
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvGNew.*dGNew(:,:,d)'));
        end
        % terms other than quadratic one
        dphiNew = U(xNew,nu,Sigma,1) + 0.5*TraceInvGdG';
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update momentum %
        %%%%%%%%%%%%%%%%%%%
        InvGMomentum = InvGNew*Momentum;
        for d = 1:D
            dQuadTerm(d) = 0.5*(InvGMomentum'*dGNew(:,:,d)*InvGMomentum);
        end
        Momentum = Momentum + (StepSize/2)*(-dphiNew + dQuadTerm');
        
    end
    
    try
        
        % Calculate proposed H value
        ProposedU = U(xNew,nu,Sigma);
        CholGNew = chol(G);
        ProposedH = ProposedU + sum(log(diag(CholGNew))) + (Momentum'*InvGNew*Momentum)/2;


        % Accept according to ratio
        Ratio = -ProposedH + CurrentH;

        if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
            x= xNew; CurrentU = ProposedU;
            CholG=CholGNew; InvG=InvGNew; dG=dGNew;
            dphi=dphiNew;
            acceptedyes = 1;
        else
            acceptedyes = 0;
        end
    catch
%         disp('Break!');
    end
    
    % Save samples if required
    if IterationNum > BurnIn
        xSaved(IterationNum-BurnIn,:) = x';
        accepted = accepted + acceptedyes;
        if mod(IterationNum,100) == 0
            disp([num2str(IterationNum) ' iterations completed.'])
            disp(accepted/(IterationNum-BurnIn));
        end
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing samples.')
        tic;
    end
    
end

TimeTaken=toc;
CurTime = fix(clock);
acpt=accepted/(NumOfIterations-BurnIn);
save(['Results/SimT_rhmc_D',num2str(D),'_K',num2str(K,'%.0e'),'_' num2str(CurTime),'.mat'],'xSaved','StepSize','NumOfLeapFrogSteps','NumOfNewtonSteps','acpt','TimeTaken')
disp(' ');
disp(['Accpetance Rate of RHMC: ',num2str(acpt)]);
disp(' ');
currentfolder=cd('./Results/');
CalculateStatistics('SimT_rhmc',strcat('D',num2str(D),'_K',num2str(K,'%.0e')));
cd(currentfolder);

end

%% sLMC


if(Alg(3))

% sampling setting
NumOfLeapFrogSteps = NLeap(3);
StepSize = Trajectory/NumOfLeapFrogSteps;

xSaved = zeros(NumOfIterations-BurnIn,D);
% Initialize
x = zeros(D,1);
% Calculate potential energy
CurrentU  = U(x,nu,Sigma);
% Calculate G
[G InvG dG Gamma1] = Met(x,nu,Sigma,[0 -1 1 3]);
CholInvG = chol(InvG);
% Calculate the partial derivatives dG/dq
for d = 1:D
    TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
end
% terms other than quadratic one
dphi = U(x,nu,Sigma,1) + 0.5*TraceInvGdG';

accepted = 0;
disp(' ');
disp('Running sLMC...');
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    xNew = x;
    GNew=G; InvGNew=InvG; CholInvGNew=CholInvG; Gamma1New=Gamma1;
    dphiNew=dphi;
    
    % propose velocity
    Velocity = (randn(1,D)*CholInvGNew)';
    
    % Calculate current H value
    CurrentLogDet = - sum(log(diag(CholInvGNew)));
    CurrentH  = CurrentU + CurrentLogDet + (Velocity'*GNew*Velocity)/2;
    
    % Accumulate determinant to be adjusted in acceptance rate
    Deltalogdet = 0;
    
    randL = ceil(rand*NumOfLeapFrogSteps);
    
    % Perform leapfrog steps
    for StepNum = 1:randL
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Multiple fixed point iteration
        PV = Velocity; dQuadTerm = zeros(D,1);
        for FixedIter = 1:NumOfNewtonSteps
            VelocityHist(FixedIter,:) = PV;
            
            for d = 1:D
                dQuadTerm(d) = PV'*Gamma1New(:,:,d)*PV;
            end
            PV = Velocity - (StepSize/2)*(InvGNew*(dphiNew + dQuadTerm));
        end
        Velocity = PV;
        
        % Record Delta log-determinant
        VGamma1 = zeros(D);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(GNew+StepSize*VGamma1));
%         Deltalogdet = Deltalogdet - 2*sum(log(diag(chol(GNew+StepSize*VGamma1))));

        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        xNew = xNew + StepSize*Velocity;
        
        % Update G based on new parameters
        [GNew InvGNew dG Gamma1New] = Met(xNew,nu,Sigma,[0 -1 1 3]);
        % Update the partial derivatives dG/dq
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvGNew.*dG(:,:,d)'));
        end
        % terms other than quadratic one
        dphiNew = U(xNew,nu,Sigma,1) + 0.5*TraceInvGdG';
        
        % Record Delta log-determinant
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(GNew-StepSize*VGamma1));
%         Deltalogdet = Deltalogdet + 2*sum(log(diag(chol(GNew-StepSize*VGamma1))));

        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
%         for d = 1:D
%             dQuadTerm(d) = Velocity'*Gamma1(:,:,d)*Velocity;
%         end
        dQuadTerm = VGamma1*Velocity;
        Velocity = Velocity - (StepSize/2)*(InvGNew*(dphiNew + dQuadTerm));
        
    end
    
    try
        % Calculate proposed H value
        ProposedU = U(xNew,nu,Sigma);
        CholInvGNew = chol(InvGNew);
        ProposedH = ProposedU - sum(log(diag(CholInvGNew))) + (Velocity'*GNew*Velocity)/2;


        % Accept according to ratio
        Ratio = -ProposedH + CurrentH + Deltalogdet;

        if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
            x= xNew; CurrentU = ProposedU;
            G=GNew; InvG=InvGNew; CholInvG=CholInvGNew; Gamma1=Gamma1New;
            dphi=dphiNew;
            acceptedyes = 1;
        else
            acceptedyes = 0;
        end
    catch
        disp('Break!');
    end
    
    % Save samples if required
    if IterationNum > BurnIn
        xSaved(IterationNum-BurnIn,:) = x';
        accepted = accepted + acceptedyes;
        if mod(IterationNum,100) == 0
            disp([num2str(IterationNum) ' iterations completed.'])
            disp(accepted/(IterationNum-BurnIn));
        end
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing samples.')
        tic;
    end
    
end

TimeTaken=toc;
CurTime = fix(clock);
acpt=accepted/(NumOfIterations-BurnIn);
save(['Results/SimT_slmc_D',num2str(D),'_K',num2str(K,'%.0e'),'_' num2str(CurTime),'.mat'],'xSaved','StepSize','NumOfLeapFrogSteps','NumOfNewtonSteps','acpt','TimeTaken')
disp(' ');
disp(['Accpetance Rate of SLMC: ',num2str(acpt)]);
disp(' ');
currentfolder=cd('./Results/');
CalculateStatistics('SimT_slmc',strcat('D',num2str(D),'_K',num2str(K,'%.0e')));
cd(currentfolder);

end


%% LMC

if(Alg(4))

% sampling settings
NumOfLeapFrogSteps = NLeap(4);
StepSize = Trajectory/NumOfLeapFrogSteps;

xSaved = zeros(NumOfIterations-BurnIn,D);
% Initialize
x = zeros(D,1);
% Calculate potential energy
CurrentU  = U(x,nu,Sigma);
% Calculate G
[G InvG dG Gamma1] = Met(x,nu,Sigma,[0 -1 1 3]);
CholInvG = chol(InvG);
% Calculate the partial derivatives dG/dq
for d = 1:D
    TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
end
% terms other than quadratic one
dphi = U(x,nu,Sigma,1)+ 0.5*TraceInvGdG';

accepted = 0;
disp(' ');
disp('Running LMC...');
for IterationNum = 1:NumOfIterations
    
    %IterationNum
    xNew = x;
    GNew=G; CholInvGNew=CholInvG; Gamma1New=Gamma1;
    dphiNew=dphi;
    
    % propose velocity
    Velocity = (randn(1,D)*CholInvGNew)';
    
    % Calculate current H value
    CurrentLogDet = sum(log(diag(CholInvGNew))); % adjusted
    CurrentH = CurrentU + CurrentLogDet + (Velocity'*GNew*Velocity)/2;
    
    % Accumulate determinant to be adjusted in acceptance rate
    Deltalogdet = 0;
    
    randL = ceil(rand*NumOfLeapFrogSteps);
    
    
    % Perform leapfrog steps
    for StepNum = 1:randL
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        VGamma1 = zeros(D);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(GNew+(StepSize/2)*VGamma1));
%         Deltalogdet = Deltalogdet - 2*sum(log(diag(chol(GNew+(StepSize/2)*VGamma1))));
        Velocity = (GNew+(StepSize/2)*VGamma1)\(GNew*Velocity- (StepSize/2)*dphiNew);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(GNew-(StepSize/2)*VGamma1));
%         Deltalogdet = Deltalogdet + 2*sum(log(diag(chol(GNew-(StepSize/2)*VGamma1))));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        % Make a full step for the position
        xNew = xNew + StepSize*Velocity;
        
        
        % Update G based on new parameters
        [GNew InvG dG Gamma1New] = Met(xNew,nu,Sigma,[0 -1 1 3]);
        % Update the partial derivatives dG/dq
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)'));
        end
        % terms other than quadratic one
        dphiNew = U(xNew,nu,Sigma,1)+ 0.5*TraceInvGdG';
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Make a half step for Velocity
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(GNew+(StepSize/2)*VGamma1));
%         Deltalogdet = Deltalogdet - 2*sum(log(diag(chol(GNew+(StepSize/2)*VGamma1))));
        Velocity = (GNew+(StepSize/2)*VGamma1)\(GNew*Velocity- (StepSize/2)*dphiNew);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1New(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(GNew-(StepSize/2)*VGamma1));
%         Deltalogdet = Deltalogdet + 2*sum(log(diag(chol(GNew-(StepSize/2)*VGamma1))));
        
    end
    
    try
        
        % Calculate proposed H value
        ProposedU = U(xNew,nu,Sigma);
        CholInvGNew = chol(InvG);
        ProposedH = ProposedU + sum(log(diag(CholInvGNew))) + (Velocity'*GNew*Velocity)/2;


        % Accept according to ratio
        Ratio = -ProposedH + CurrentH + Deltalogdet;

        if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
            x= xNew; CurrentU = ProposedU;
            G=GNew; Gamma1=Gamma1New; CholInvG=CholInvGNew;
            dphi=dphiNew;
            acceptedyes = 1;
        else
            acceptedyes = 0;
        end
    catch
        disp('Break!');
    end
    
    % Save samples if required
    if IterationNum > BurnIn
        xSaved(IterationNum-BurnIn,:) = x';
        accepted = accepted + acceptedyes;
        if mod(IterationNum,100) == 0
            disp([num2str(IterationNum) ' iterations completed.'])
            disp(accepted/(IterationNum-BurnIn));
        end
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing samples.')
        tic;
    end
    
end

TimeTaken=toc;
CurTime = fix(clock);
acpt=accepted/(NumOfIterations-BurnIn);
save(['Results/SimT_lmc_D',num2str(D),'_K',num2str(K,'%.0e'),'_' num2str(CurTime),'.mat'],'xSaved','StepSize','NumOfLeapFrogSteps','acpt','TimeTaken')
disp(' ');
disp(['Accpetance Rate of LMC: ',num2str(acpt)]);
disp(' ');
currentfolder=cd('./Results/');
CalculateStatistics('SimT_lmc',strcat('D',num2str(D),'_K',num2str(K,'%.0e')));
cd(currentfolder);

end



end



%% functions needed
function [output] = U(x,nu,Sigma,der)
if(nargin<4)
    der=0;
end

D=length(x);
InvSigmax=Sigma\x;
brakt=nu+x'*InvSigmax;

if der==0
    output = (nu+D)/2*log(brakt/nu);
elseif der==1
    output = (nu+D)/brakt.*InvSigmax;
else
    disp('wrong choice of der!')
end

end


function [G InvG dG Gamma1] = Met(x,nu,Sigma,opt)
if(nargin<4)
    opt=0;
end

global InvSigma
D=length(x);
InvSigmax=InvSigma*x;
brakt=nu+x'*InvSigmax;

InvSigmax2=InvSigmax*InvSigmax';

G = -2*(nu+D)/brakt^2.*InvSigmax2 + (nu+D)/brakt.*InvSigma;
if all(opt==0)
    InvG=NaN; dG=NaN; Gamma1=NaN;
else
    if any(opt==-1)
%         InvG = inv(G);
        InvG = 1/(nu+D)*brakt.*(Sigma+1/(nu-brakt/2).*(x*x'));
    end
    if any(opt==1)
        dG1 = 8*(nu+D)/brakt^3.*reshape(kron(InvSigmax',InvSigmax2),repmat(D,1,3));
        dG2 = reshape(kron(InvSigmax',InvSigma),repmat(D,1,3));
        dG2 = 2*(nu+D)/brakt^2.*(dG2 + permute(dG2,[1,3,2]) + permute(dG2,[3,2,1]));
        dG = dG1 - dG2;
        if any(opt==3)
            Gamma1 = dG;
        else
            Gamma1=NaN;
        end
    else
        dG=NaN; Gamma1=NaN;
    end
end

end

