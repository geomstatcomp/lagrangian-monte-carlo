Lagrangian Monte Carlo (LMC) improves the computational efficiency of 
Riemannian Hamiltonian Monte Carlo (RHMC, Girolami and Calderhead 2011).
It makes proposals by simulating the Langevin equation defined on the tangent space, 
instead of the cotangent space of the target manifold.
Explicit, rather than implicit, numerical integrators are derived and analyzed for LMC.
Bother numerical efficiency and numerical stability are improved over RHMC.

This repo contains Matlab codes for examples in the following paper:

Shiwei Lan, Vasileios Stathopoulos, Babak Shahbaba & Mark Girolami
Markov Chain Monte Carlo from Lagrangian Dynamics
Journal of Computational and Graphical Statistics, Volume 24, Issue 2, Pages 357-378, 2015
http://www.tandfonline.com/doi/abs/10.1080/10618600.2014.902764

CopyRight: Shiwei Lan and Vasileios Stathopoulos

Please cite the reference when using the codes, Thanks!

Shiwei Lan
9-18-2016