clear;
clc;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% To run this script on a SGE cluster
% use a batch job with 150 tasks.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FOR SGE CLUSTER
taskID = str2num(getenv('SGE_TASK_ID'));
%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% To run this script on a single machine
% un-comment the "for" bellow and the 
% corresponding "end" at the end of this file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% FOR SINGLE MACHINE
for taskID = 1:100
%%%%
    
% EXPERIMENT DESIGN
mcmc =  {'hmc','rmhmc', 'ermlmc','rmlmc'};
data = {'claw','trimodal','skewed','kurtotic','bimodal'};
nRepeats = 5;

levels = [ length(mcmc) length(data) nRepeats];

dataLoc = './data/';
outputFilePrefix = [];

% get task settings
settings = factsett(levels, taskID);

%seed the random number generator
randStream = RandStream('mt19937ar','Seed',taskID);
RandStream.setGlobalStream(randStream);

%LOAD DATASET & CREATE MODEL
dataset = [dataLoc data{settings(2)} '.mat'];

switch (data{settings(2)} )
    case {'kurtotic', 'bimodal', 'skewed'}
        K = 2;
    case 'trimodal'
        K = 3;
    case 'claw'
        K = 6;
end
load(dataset,'X');

% prior hyper-parameters
dirichlet_prior = 1;
means_Normal_mean = mean(X);
means_Normal_var = 4*var(X);
var_Igamma_shape = 2;
var_Igamma_scale = 0.5;

% create target posterior
gmm_model = uGMMTarget(X, K, dirichlet_prior, means_Normal_mean, ...
    means_Normal_var, var_Igamma_shape, var_Igamma_scale);

% Get initial model parameters
x0  = gmm_model.InitParameters;

% SAMPLER SETTINGS
Samples = 5000;
VerboseLvl = 'all';
Adapt = 100;

switch mcmc{settings(1)}
    
     case 'hmc'
        % === RMHMC ===
        Burnin = 10000;
        LeapFrogs = 10;
        StepSize = 0.1;
        
        mcmc_sampler = MCMCSampler.NewHMCSampler(gmm_model,...
            'Samples',Samples,'BurnIn',Burnin,...
            'StepSize',StepSize, 'LeapFrogSteps',LeapFrogs, ...
            'VerboseLevel',VerboseLvl,'Adapt',Adapt,'Init',x0);
    
    case 'rmhmc'
        % === RMHMC ===
        Burnin = 5000;
        LeapFrogs = 10;
        StepSize = 0.1;
        
        mcmc_sampler = MCMCSampler.NewRMHMCSampler(gmm_model,...
            'Samples',Samples,'BurnIn',Burnin,...
            'StepSize',StepSize, 'LeapFrogSteps',LeapFrogs, ...
            'VerboseLevel',VerboseLvl,'Adapt',Adapt,'Init',x0);
        
    case 'ermlmc'
        % === Explicit RMLMC ===
        Burnin = 5000;
        LeapFrogs = 10;
        StepSize = 0.1;
        
        mcmc_sampler = MCMCSampler.NewERMLMCKernel(gmm_model,...
            'Samples',Samples,'BurnIn',Burnin,...
            'StepSize',StepSize, 'LeapFrogSteps',LeapFrogs, ...
            'VerboseLevel',VerboseLvl,'Adapt',Adapt,'Init',x0);
        
    case 'rmlmc'
        % === Semi-Explicit RMLMC ===
        Burnin = 5000;
        LeapFrogs = 10;
        StepSize = 0.1;
              
        mcmc_sampler = MCMCSampler.NewRMLMCSampler(gmm_model,...
            'Samples',Samples,'BurnIn',Burnin,...
            'StepSize',StepSize, 'LeapFrogSteps',LeapFrogs, ...
            'VerboseLevel',VerboseLvl,'Adapt',Adapt,'Init',x0);
        
    otherwise
        error('clusterExoLogReg:TaskIDError','TaskID out of range');
end


% LOG EXPERIMENT
fprintf(1,'======================================================\n');
fprintf(1,'MCMC: %s data: %s Repetition %u \n',...
    mcmc{settings(1)},...
    data{settings(2)},...
    settings(3) );
mcmc_sampler.printStats();

fprintf(1,'======================================================\n');

% START SAMPLING
[chain timeTaken accRate burnIn] = mcmc_sampler.MCMCSample();

%LOG SAMPLER STATS
fprintf(1,'======================================================\n');
mcmc_sampler.printStats();
fprintf(1,'======================================================\n');

% SAVE & EXIT
 outputFile = ['./results/' outputFilePrefix mcmc{settings(1)} '_' data{settings(2)} '_' num2str(settings(3)) '.mat'];
 mcmcParams = mcmc_sampler.getKernelParameters();
 save(outputFile, 'chain','timeTaken','burnIn','mcmcParams','accRate');

 
%%%% FOR SINGLE MACHINE
 end
%%%%

