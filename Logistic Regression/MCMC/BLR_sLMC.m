function [ ] = BLR_sLMC( DataSet )

% Random Numbers...
randn('state', 2012);
rand('twister', 2012);

switch(DataSet)
    
    case 'Australian'
    
        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/australian.mat');
        t=X(:,end);
        X(:,end)=[];
        
        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);

        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 7;
        StepSize           = 4.5/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
    case 'German'

        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/german.mat');
        t=X(:,end);
        X(:,end)=[];

        % German Credit - replace all 1s in t with 0s
        t(find(t==1)) = 0;
        % German Credit - replace all 2s in t with 1s
        t(find(t==2)) = 1;

        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 5;
        StepSize           = 4/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
    case 'Heart'
        
        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/heart.mat');
        t=X(:,end);
        X(:,end)=[];

        % German Credit - replace all 1s in t with 0s
        t(find(t==1)) = 0;
        % German Credit - replace all 2s in t with 1s
        t(find(t==2)) = 1;
        
        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 6;
        StepSize           = 4.5/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;

        
    case 'Pima'

        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        load('./Data/pima.mat');
        t=X(:,end);
        X(:,end)=[];

        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end

        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 6;
        StepSize           = 5/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
        
        
    case 'Ripley'

        %Two hyperparameters of model
        Polynomial_Order = 3;
        alpha=100;

        %Load and prepare train & test data
        load('./Data/ripley.mat');
        t=X(:,end);
        X(:,end)=[];
        
        N = size(X,1);
        
        % Standardise Data
        X = (X-repmat(mean(X),N,1))./repmat(std(X),N,1);
        
        %Create Polynomial Basis
        %
        XX = ones(size(X,1),1);
        for i = 1:Polynomial_Order
            XX = [XX X.^i];
        end
        
        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 7;
        StepSize           = 3.5/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
    case 'CTG'
    
        %Two hyperparameters of model
        Polynomial_Order = 1;
        alpha=100; 

        %Load and prepare train & test data
        %data set is already standardized
        load('./Data/CTG.mat');
        t = y;
        XX = x;
        [N,D] = size(XX);
        
        % HMC Setup
        NumOfIterations    = 20000;
        BurnIn             = 5000;
        NumOfLeapFrogSteps = 7;
        StepSize           = 3/NumOfLeapFrogSteps;
        NumOfNewtonSteps   = 5;
        
end


Proposed = 0;
Accepted = 0;

% Set initial values of q
q = ones(D,1)*1e-3;
qSaved = zeros(NumOfIterations-BurnIn,D);
% loglik = zeros(NumOfIterations-BurnIn,1);
warning off

% Pre-allocate memory for partial derivatives
% for d = 1:D
%     GDeriv{d} = zeros(D);
% end


% Calculate joint log likelihood for current q
CurrentU    = U(t,XX,q,alpha);


for IterationNum = 1:NumOfIterations
    
    if mod(IterationNum,100) == 0
        disp([num2str(IterationNum) ' iterations completed.'])
        disp(Accepted/Proposed)
        Proposed = 0;
        Accepted = 0;
        drawnow
    end
    
    %IterationNum
    qNew = q;
    Proposed = Proposed + 1;
    
    % random number of leapfrog steps
    if (randn > 0.5) TimeStep = 1; else TimeStep = -1; end
    %TimeStep = 1;
    %RandomSteps = ceil(rand*NumOfLeapFrogSteps);
    SavedSteps(1,:) = qNew;
    
    % pre-leapfrog calculation
    % Calculate G
    G     = Met(XX,qNew,alpha);
    InvG  = inv(G); CholG = chol(G);
    CholInvG = chol(InvG);
    % Calculate the partial derivatives dG/dq
    dG = Met(XX,qNew,alpha,1);
    for d = 1:D
        TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)));
    end
    Gamma1 = .5*dG; % 1st Christoffel Symbol, symmectric in dim i,j,k for LR problem
    % terms other than quadratic one
    dphi = U(t,XX,qNew,alpha,1) + 0.5*TraceInvGdG';
    
    % propose velocity
    Velocity = (randn(1,D)*CholInvG)';
    
    % Calculate current H value
    CurrentLogDet = - sum(log(diag(CholInvG)));
    CurrentH  = CurrentU + CurrentLogDet + (Velocity'*G*Velocity)/2;
    
    % Accumulate determinant to be adjusted in acceptance rate
    Deltalogdet = 0;
    
    % Perform leapfrog steps
    for StepNum = 1:NumOfLeapFrogSteps
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
        % Multiple fixed point iteration
        PV = Velocity; dQuadTerm = zeros(D,1);
        for FixedIter = 1:NumOfNewtonSteps
            VelocityHist(FixedIter,:) = PV;
            
            for d = 1:D
                dQuadTerm(d) = PV'*Gamma1(:,:,d)*PV;
            end
            PV = Velocity - (TimeStep*StepSize/2)*(InvG*(dphi + dQuadTerm));
        end
        Velocity = PV;
        
        % Record Delta log-determinant
        VGamma1 = zeros(D);
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1(:,:,k);
        end
        Deltalogdet = Deltalogdet - log(det(G+(TimeStep*StepSize)*VGamma1));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%
        % Update q parameters %
        %%%%%%%%%%%%%%%%%%%%%%%
        qNew = qNew + (TimeStep*StepSize)*Velocity;
        
        
        % Update G based on new parameters
        G = Met(XX,qNew,alpha);
        InvG = inv(G); %CholG = chol(G);
        % Update the partial derivatives dG/dq
        dG = Met(XX,qNew,alpha,1);
        for d = 1:D
            TraceInvGdG(d) = sum(sum(InvG.*dG(:,:,d)));
        end
        Gamma1 = .5*dG;
        % terms other than quadratic one
        dphi = U(t,XX,qNew,alpha,1) + 0.5*TraceInvGdG';
        
        % Record Delta log-determinant
        for k=1:D
            VGamma1(k,:) = Velocity'*Gamma1(:,:,k);
        end
        Deltalogdet = Deltalogdet + log(det(G-(TimeStep*StepSize)*VGamma1));
        
        
        
        %%%%%%%%%%%%%%%%%%%
        % Update velocity %
        %%%%%%%%%%%%%%%%%%%
%         for d = 1:D
%             dQuadTerm(d) = Velocity'*Gamma1(:,:,d)*Velocity;
%         end
        dQuadTerm = VGamma1*Velocity;
        Velocity = Velocity - (TimeStep*StepSize/2)*(InvG*(dphi + dQuadTerm));
        
        % save all proposals
        SavedSteps( StepNum + 1, : ) = qNew;
        
        
    end
    
    try
        % Calculate proposed H value
        ProposedLogDet = sum(log(diag(chol(G))));
        ProposedU = U(t,XX,qNew,alpha);

        ProposedH = ProposedU + ProposedLogDet + (Velocity'*G*Velocity)/2;


        % Accept according to ratio
        Ratio = -ProposedH + CurrentH + Deltalogdet;

        if (isfinite(Ratio) & (Ratio > min([0,log(rand)])))
            CurrentU = ProposedU;
            q = qNew;
            Accepted = Accepted + 1;
    %        disp('Accepted')
        else
    %        disp('Rejected')
        end
    catch
        disp('Rejected');
        drawnow
    end
    
    
    % Save samples if required
    if IterationNum > BurnIn
        qSaved(IterationNum-BurnIn,:) = q;
%         f = XX*q; loglik(IterationNum-BurnIn) = f'*t - sum(log(1+exp(f)));
    end
    
    % Start timer after burn-in
    if IterationNum == BurnIn
        disp('Burn-in complete, now drawing posterior samples.')
        tic;
    end
    
end

% Stop timer
TimeTaken = toc
betaPosterior = qSaved;
acprat = size(unique(betaPosterior,'rows'),1)/(NumOfIterations-BurnIn)

CurTime = fix(clock);
cd('..')
save(['Results/Results_sLMC_lr_' num2str(NumOfLeapFrogSteps) '_' num2str(NumOfNewtonSteps) '_' DataSet '_' num2str(CurTime) '.mat'], 'StepSize', 'NumOfLeapFrogSteps', 'betaPosterior', 'acprat', 'TimeTaken')

% Plot paths and histograms
% figure(1)
% 
% subplot(1,3,1);
% plot(qSaved);
% 
% for d=1:D
%     subplot(1,3,1+d);
%     [f,x]=hist(qSaved(:,d),50);
%     bar(x,f/trapz(x,f));hold on
%     [f,xi] = ksdensity(qSaved(:,d));
%     plot(xi,f,'r');hold off
% end

end




function [output]=U(t,XX,q,alpha,der)
if(nargin<5)
    der=0;
end

f  = XX*q;
D = length(q);

if der==0
    % Calculate joint log likelihood for q
%     LogPrior      = -ecmnobj(q',zeros(D,1),alpha*eye(D));
    LogPrior      = -q'*q/2/alpha;
    LogLikelihood = f'*t - sum(log(1+exp(f))); %training likelihood
    output    = -(LogLikelihood + LogPrior);
elseif der==1
    % Calculate gradient of joint log likelihood for q
    dLogPrior      = - (1/alpha)*q ;
    dLogLikelihood = XX'*( t - (1./(1+exp(-f))));
    output    = -(dLogLikelihood + dLogPrior);
else
    disp('wrong choice of der!')
end

end




function [output]=Met(XX,q,alpha,der)
if(nargin<4)
    der=0;
end

[N,D] = size(XX);
f = XX*q;
p = 1./(1+exp(-f));
v = p.*(ones(N,1)-p);

% Calculate G
if der==0
    G = (XX'.*repmat(v',D,1))*XX + (eye(D)./alpha);
    output = G;
% Calculate the partial derivatives dG/dq
% Faster to compute Z as vector then diag in the next calculation
elseif der==1
    dG = zeros(repmat(D,1,3));
    for d = 1:D
        Z = ((1 - 2*p).*XX(:,d));
        %GDeriv{d} = XX'*v*diag(Z)*XX; % Slow because of diag
        % faster
        Z1 = (v.*Z);
        for a =1:D Z2(:,a) = (XX(:,a).*Z1); end
        dG(:,:,d) = Z2'*XX;
    end
    output = dG;
else
    disp('wrong choice of der!')
end


end
